      SUBROUTINE XTCHEMPAM(XROW,TH,QV, &
                           SGPP,DSHJ,SHJ,CSZROW,PRESSG,IAIND,NTRACP, &
                           SFRC,OHROW,H2O2ROW,O3ROW,NO3ROW, &
                           DOX4ROW,DOXDROW,NOXDROW,ZCLF,ZMRATEP,ZFSNOW, &
                           ZFRAIN,ZMLWC,ZFEVAP,CLRFR,CLRFS,ZFSUBL,ATAU, &
                           ITRWET,ISO2,IDMS,IHPO,ZTMST,NLATJ, &
                           NTRAC,ILG,IL1,IL2,ILEV)
!
!     * JUNE 2, 2013 - K.VONSALZEN. PLA VERSION OF XTCHEMIE.
!
      USE SDPHYS
!
      IMPLICIT NONE
!
      INTEGER, INTENT(IN) :: ISO2,IDMS,IHPO,NTRAC,ILG,IL1,IL2,ILEV,NLATJ,NTRACP
      REAL, INTENT(IN) :: ZTMST

      REAL, DIMENSION(ILG,ILEV,NTRAC), INTENT(INOUT) :: XROW, SFRC
      REAL, DIMENSION(ILG,ILEV      ), INTENT(IN)    :: TH, QV, DSHJ, SHJ
      REAL, DIMENSION(ILG           ), INTENT(IN)    :: CSZROW, PRESSG
      REAL, DIMENSION(ILG           ), INTENT(OUT)   :: ATAU

! SPECIES PASSED IN VMR (DIMENSIONLESS).
      REAL, DIMENSION(ILG,ILEV      ), INTENT(IN)    :: OHROW, H2O2ROW, O3ROW, NO3ROW
!
      REAL, DIMENSION(ILG           ), INTENT(OUT)   :: DOX4ROW, DOXDROW, NOXDROW

! ARRAYS SHARED WITH COND IN PHYSICS.
      REAL, DIMENSION(ILG,ILEV      ), INTENT(IN)    :: ZCLF, ZFRAIN, CLRFR, ZFSNOW, ZFEVAP, CLRFS, ZFSUBL
      REAL, DIMENSION(ILG,ILEV      ), INTENT(INOUT) :: ZMRATEP, ZMLWC

! CONTROL INDEX ARRAYS FOR TRACERS.
      INTEGER, INTENT(IN) :: ITRWET(NTRAC)
!
!     * INTERNAL WORK ARRAYS:
!
!     * GENERAL WORK ARRAYS FOR XTCHEMIE5
!
      REAL  ,  DIMENSION(ILG,ILEV,NTRAC)  :: ZXTE, XCLD, XCLR
      REAL  ,  DIMENSION(ILG,ILEV)  :: ZXTP10,ZXTP1C,ZHENRY, &
                                       ZZOH,ZZH2O2,ZZO3,ZZO2, &
                                       ZZNO3,AGTHPO,ZRHO0,ZRHCTOX, &
                                       ZRHXTOC,ZDEP3D,PDCLR,PDCLD, &
                                       TMPSCL,XAROW,XCROW,DXCDT,DXADT, &
                                       DXDT
      REAL  ,  DIMENSION(ILG)  :: ZXTP1,ZDAYL
      REAL  ,  DIMENSION(ILG,ILEV), INTENT(OUT) :: SGPP
!
!     * WORK ARRAYS USED IN OXISTR3 ONLY.
!
      INTEGER, PARAMETER :: NAGE=1
      INTEGER, PARAMETER :: NEQP=13
      INTEGER, DIMENSION(NTRACP)    :: IAIND
!
      REAL, PARAMETER :: YTAU   = 1800.
      REAL, PARAMETER :: ZCTHR  = 0.99
!       REAL, PARAMETER :: YSMALL = 9.E-30
      REAL, PARAMETER :: YSMALL = 1e-8          ! ??? smaller value causes division by zero
      REAL, PARAMETER :: ZERO = 0
!-----------------------------------------------------------------------
! Constants
      REAL, PARAMETER :: ZMIN     = 1.E-20
      REAL, PARAMETER :: ASRSO4   = 0.9
      REAL, PARAMETER :: ASRPHOB  = 0.0
      REAL, PARAMETER :: ZK2I     = 2.0E-12
      REAL, PARAMETER :: ZK2      = 4.0E-31
      REAL, PARAMETER :: ZK2F     = 0.45
      REAL, PARAMETER :: ZK3      = 1.9E-13
      REAL, PARAMETER :: ZMOLGS   = 32.064
      REAL, PARAMETER :: ZMOLGAIR = 28.84
      REAL, PARAMETER :: ZAVO     = 6.022045E+23
      REAL, PARAMETER :: ZNAMAIR  = 1.E-03*ZAVO/ZMOLGAIR
      REAL, PARAMETER :: YSPHR    = 3600.
!=======================================================================
!     * DEFINE FUNCTION FOR CHANGING THE UNITS
!     * FROM MASS-MIXING RATIO TO MOLECULES PER CM**3 AND VICE VERSA
!
      REAL :: XTOC, CTOX, X, Y
      XTOC(X,Y) = X*ZAVO*1e3/Y
      CTOX(X,Y) = Y/(ZAVO*1e3*X)
!
!     * X = DENSITY OF AIR, Y = MOL WEIGHT IN GRAMM
!
!       ZFARR(ZK,ZH,ZTPQ)=ZK*EXP(ZH*ZTPQ)

! Local arrays
      INTEGER :: IL,JK,JT,LONSL,II,JJ
      REAL :: FACTCON, PCONS2, PQTMST, ZVDAYL, ZXTP1SO2, ZSO2, ZTK2, ZHIL, ZTK23B
      REAL :: ZEXP, ZM, ZDMS, ZXTP1DMS, T, ZTK1, ZTKA, ZTKB, SCALF, FAC, ZTK3
      LOGICAL, DIMENSION(ILG,ILEV) :: ZCLFT, SFRCN, SFRCP


      PCONS2 = 1./(ZTMST*GRAV)
      PQTMST = 1./ZTMST
!
!     * DEFINE CONSTANT 2-D SLICES.
!
      DO JK=1,ILEV
          DO IL=IL1,IL2
            ZRHO0  (IL,JK)=SHJ(IL,JK)*PRESSG(IL)/(RGAS*TH(IL,JK))
            ZRHXTOC(IL,JK)=XTOC(ZRHO0(IL,JK),ZMOLGS)
            ZRHCTOX(IL,JK)=CTOX(ZRHO0(IL,JK),ZMOLGS)
          ENDDO
      ENDDO
!
!     * OXIDANT CONCENTRATIONS.
!
      DO JK=1,ILEV
         DO IL=IL1,IL2
!
!       * Convert to molecules/cm3 from VMR
!
            FACTCON = ZRHO0(IL,JK)*ZNAMAIR
            ZZOH  (IL,JK)=OHROW  (IL,JK)*FACTCON
            ZZH2O2(IL,JK)=H2O2ROW(IL,JK)*FACTCON
            ZZO3  (IL,JK)=O3ROW  (IL,JK)*FACTCON
            ZZNO3 (IL,JK)=NO3ROW (IL,JK)*FACTCON
!
!       * Oxygen concentration in molecules/cm3
!
            ZZO2  (IL,JK)=0.21*1.E-06*FACTCON
         ENDDO
      ENDDO
      SGPP=0.
!
!     * BACKGROUND AGING TIME SCALE FOR CARBONACEOUS AEROSOL.
!
      IF( NAGE.EQ.0 ) THEN
        ATAU=36.*YSPHR
      ELSE
        ATAU=12.*YSPHR
      ENDIF
!-----------------------------------------------------------------
!     * HYDROGEN PEROXIDE PRODUCTION AND INITIALIZATION.
!
!       DO JK=1,ILEV
!         DO IL=IL1,IL2
! ! convert molecules/cm**3 to kg-S/kg
!           AGTHPO(IL,JK) = ZZH2O2(IL,JK) * 1.E+06 * ZMOLGS * 1.E-3 / (ZAVO * ZRHO0(IL,JK))
!         ENDDO
!       ENDDO
      AGTHPO(IL1:IL2,:) = ZZH2O2(IL1:IL2,:) * 1.E+06 * ZMOLGS * 1.E-3 / (ZAVO * ZRHO0(IL1:IL2,:))
!
!     * INITIAL HYDROGEN PEROXIDE MIXING RATIOS FOR CLEAR AND CLOUDY SKY.
!

!       TMPSCL(IL1:IL2,:)=1.
      WHERE ( (ZCLF(IL1:IL2,:) < ZCTHR) .AND. ((1.-ZCLF(IL1:IL2,:)) < ZCTHR) )
        WHERE     ( SFRC(IL1:IL2,:,IHPO) < -YSMALL )
          TMPSCL(IL1:IL2,:)=-XROW(IL1:IL2,:,IHPO)*(1.-ZCLF(IL1:IL2,:))/SFRC(IL1:IL2,:,IHPO)
        ELSEWHERE ( SFRC(IL1:IL2,:,IHPO) >  YSMALL )
          TMPSCL(IL1:IL2,:)= XROW(IL1:IL2,:,IHPO)*    ZCLF(IL1:IL2,:) /SFRC(IL1:IL2,:,IHPO)
        ELSEWHERE
          TMPSCL(IL1:IL2,:)=0.
        ENDWHERE
      ELSEWHERE
        TMPSCL(IL1:IL2,:)=1.
      ENDWHERE

!       ZCLFT = (ZCLF < ZCTHR) .AND. ((1.-ZCLF) < ZCTHR)
!       SFRCN = ( SFRC(:,:,IHPO) < -YSMALL )
!       SFRCP = ( SFRC(:,:,IHPO) >  YSMALL )
!       TMPSCL(ZCLFT) = 0.
!       TMPSCL(.NOT. ZCLFT) = 1.0
!       TMPSCL(ZCLFT .AND. SFRCN) = -XROW(:,:,IHPO)*(1.-ZCLF)/SFRC(:,:,IHPO)
!       TMPSCL(ZCLFT .AND. SFRCP) =  XROW(:,:,IHPO)*(1.-ZCLF)/SFRC(:,:,IHPO)

!       DO IL=IL1,IL2
!           DO JK=1,ILEV
!               IF ( ZCLF(IL,JK) < ZCTHR .AND. (1.-ZCLF(IL,JK)) < ZCTHR ) THEN
!                 IF     ( SFRC(IL,JK,IHPO) < -YSMALL ) THEN
!                   TMPSCL(IL,JK)=-XROW(IL,JK,IHPO)*(1.-ZCLF(IL,JK))/SFRC(IL,JK,IHPO)
!                 ELSEIF ( SFRC(IL,JK,IHPO) >  YSMALL ) THEN
!                   if (abs(SFRC(IL,JK,IHPO))<=YSMALL) then
!                       print '("In PAM-chem1: ",3i4,2es12.4,f6.3)',IL,JK,JT,SFRC(IL,JK,IHPO),YSMALL,ZCLF(IL,JK)               ! shouldn't be! but... check YSMALL?
!                       TMPSCL(IL,JK)=0.
!                   else
!                       TMPSCL(IL,JK)= XROW(IL,JK,IHPO)*    ZCLF(IL,JK) /SFRC(IL,JK,IHPO)
!                   endif
!                 ELSE
!                   TMPSCL(IL,JK)=0.
!                 ENDIF
!               ELSE
!                   TMPSCL(IL,JK)=1.
!               ENDIF
!           ENDDO
!       ENDDO

      TMPSCL(IL1:IL2,:)=MAX(MIN(TMPSCL(IL1:IL2,:),1.),0.)
      SFRC(IL1:IL2,:,IHPO)=TMPSCL(IL1:IL2,:)*SFRC(IL1:IL2,:,IHPO)
      XAROW=XROW(IL1:IL2,:,IHPO)
      XCROW=XROW(IL1:IL2,:,IHPO)
      WHERE ( (ZCLF(IL1:IL2,:) < ZCTHR) .AND. ((1.-ZCLF(IL1:IL2,:)) < ZCTHR) )
        XAROW(IL1:IL2,:)=XAROW(IL1:IL2,:)+SFRC(IL1:IL2,:,IHPO)/(1.-ZCLF(IL1:IL2,:))
        XCROW(IL1:IL2,:)=XCROW(IL1:IL2,:)-SFRC(IL1:IL2,:,IHPO)/    ZCLF(IL1:IL2,:)
      ENDWHERE

!     * CHANGE IN CONCENTRATION IN CLOUDY AND CLEAR PORTIONS
!     * OF THE GRID CELL BY ADJUSTING CONCENTRATIONS TOWARDS BACKGROUND
!     * VALUES.
      DXCDT(IL1:IL2,:)=-(1./MAX(YTAU,ZTMST))*(XCROW(IL1:IL2,:)-AGTHPO(IL1:IL2,:))
      DXADT(IL1:IL2,:)=-(1./MAX(YTAU,ZTMST))*(XAROW(IL1:IL2,:)-AGTHPO(IL1:IL2,:))
      XCROW(IL1:IL2,:)=XCROW(IL1:IL2,:)+DXCDT(IL1:IL2,:)*ZTMST
      XAROW(IL1:IL2,:)=XAROW(IL1:IL2,:)+DXADT(IL1:IL2,:)*ZTMST

!     * ALL-SKY CHANGE.
      DXDT(IL1:IL2,:)=ZCLF(IL1:IL2,:)*DXCDT(IL1:IL2,:)+(1.-ZCLF(IL1:IL2,:))*DXADT(IL1:IL2,:)
      XROW(IL1:IL2,:,IHPO)=XROW(IL1:IL2,:,IHPO)+DXDT(IL1:IL2,:)*ZTMST

!     * ADJUST CLEAR/CLOUDY CONCENTRATION DIFFERENCE TO ACCOUNT
!     * FOR PRODUCTION OF H2O2 IN CLOUDY PORTION OF THE GRID CELL.
      SFRC(IL1:IL2,:,IHPO)=0.
      WHERE ( ZCLF(IL1:IL2,:) < ZCTHR .AND. (1.-ZCLF(IL1:IL2,:)) < ZCTHR )
        SFRC(IL1:IL2,:,IHPO)=ZCLF(IL1:IL2,:)*(1.-ZCLF(IL1:IL2,:))*(XAROW(IL1:IL2,:)-XCROW(IL1:IL2,:))
      ENDWHERE
!
      ZXTE(:,:,:)=0.
!
      DO JK=1,ILEV
        DO IL=IL1,IL2
          ZHENRY(IL,JK)=0.
          IF(ZCLF(IL,JK).LT.1.E-04.OR.ZMLWC(IL,JK).LT.ZMIN) THEN
            ZMRATEP(IL,JK)=0.
            ZMLWC(IL,JK)=0.
          ENDIF
        ENDDO
      ENDDO
!
!     * PROCESSES WHICH ARE DIFERENT INSIDE AND OUTSIDE OF CLOUDS.
!
      DO 354 JT=1,NTRAC
       IF(ITRWET(JT).NE.0 .AND. JT.NE.ISO2 .AND. JT.NE.IHPO &
        .AND. (JT.LT.IAIND(1) .OR. JT.GT.IAIND(NTRACP))) THEN
        DO JK=1,ILEV
         DO IL=IL1,IL2
!
!          * DIAGNOSE INITIAL MIXING RATIOS FOR CLEAR AND CLOUDY SKY.
!
           IF ( ZCLF(IL,JK) < ZCTHR .AND. (1.-ZCLF(IL,JK)) < ZCTHR ) THEN
             IF     ( SFRC(IL,JK,JT) < -YSMALL ) THEN
               TMPSCL(IL,JK) = -XROW(IL,JK,JT)*(1.-ZCLF(IL,JK))/SFRC(IL,JK,JT)
             ELSEIF ( SFRC(IL,JK,JT) >  YSMALL ) THEN
                  if (abs(SFRC(IL,JK,JT))<=YSMALL) then
                   print '("In PAM-chem2: ",3i4,2es12.4,f6.3)',IL,JK,JT,SFRC(IL,JK,JT),YSMALL,ZCLF(IL,JK)               ! shouldn't be! but... check YSMALL?
                   TMPSCL(IL,JK) = 0.
               else
                   TMPSCL(IL,JK) =  XROW(IL,JK,JT)*    ZCLF(IL,JK) /SFRC(IL,JK,JT)
               endif
             ELSE
               TMPSCL(IL,JK)=0.
             ENDIF
           ELSE
             TMPSCL(IL,JK)=1.
           ENDIF
           TMPSCL(IL,JK)=MAX(MIN(TMPSCL(IL,JK),1.),0.)
           SFRC(IL,JK,JT)=TMPSCL(IL,JK)*SFRC(IL,JK,JT)
           ZXTP10(IL,JK)=XROW(IL,JK,JT)
           ZXTP1C(IL,JK)=XROW(IL,JK,JT)
           IF ( ZCLF(IL,JK) < ZCTHR .AND. (1.-ZCLF(IL,JK)) < ZCTHR ) THEN
             ZXTP10(IL,JK)=ZXTP10(IL,JK)+SFRC(IL,JK,JT)/(1.-ZCLF(IL,JK))
             ZXTP1C(IL,JK)=ZXTP1C(IL,JK)-SFRC(IL,JK,JT)/ZCLF(IL,JK)
           ENDIF
           ZDEP3D(IL,JK)=0.
         ENDDO
        ENDDO
!
        IF(ITRWET(JT).EQ.1) THEN
         DO 334 JK=1,ILEV
         DO 334 IL=IL1,IL2
           ZHENRY(IL,JK)=ASRSO4
  334    CONTINUE
        ELSE
         DO 335 JK=1,ILEV
         DO 335 IL=IL1,IL2
           ZHENRY(IL,JK)=ASRPHOB
  335    CONTINUE
        ENDIF
!
        CALL WETDEP4(ILG,IL1,IL2,ILEV,ZTMST, PCONS2, ZXTP10, &
                     ZXTP1C,DSHJ,SHJ,PRESSG,TH,QV, ZMRATEP,ZMLWC, &
                     ZFSNOW,ZFRAIN,ZDEP3D,ZCLF,CLRFR,ZHENRY, &
                     CLRFS,ZFEVAP,ZFSUBL,PDCLR,PDCLD)
        DO JK=1,ILEV
            DO IL=IL1,IL2
                ZXTP1(IL)=(1.-ZCLF(IL,JK))*ZXTP10(IL,JK)+ ZCLF(IL,JK)*ZXTP1C(IL,JK)
                ZXTP1(IL)=ZXTP1(IL)-ZDEP3D(IL,JK)
                ZXTE(IL,JK,JT)=(ZXTP1(IL)-XROW(IL,JK,JT))*PQTMST
                XCLD(IL,JK,JT)=ZXTP1C(IL,JK)-PDCLD(IL,JK)
                XCLR(IL,JK,JT)=ZXTP10(IL,JK)-PDCLR(IL,JK)
            ENDDO
        ENDDO
!
       ENDIF
  354 CONTINUE
!
!     * CALCULATE THE DAY-LENGTH.
!
      LONSL=IL2/NLATJ
      DO JJ=1,NLATJ
        ZVDAYL=0.
        DO II=1,LONSL
          IL=II+(JJ-1)*LONSL
          IF(CSZROW(IL).GT.0.) THEN
            ZVDAYL=ZVDAYL+1.
          ENDIF
        ENDDO
        IF(ZVDAYL.GT.0.) ZVDAYL=REAL(LONSL)/ZVDAYL
!
        DO II=1,LONSL
          IL=II+(JJ-1)*LONSL
          ZDAYL(IL)=ZVDAYL
        ENDDO
      ENDDO
!
!     * DAY-TIME CHEMISTRY.
!
      DOX4ROW=0.
      DOXDROW=0.
      NOXDROW=0.
      DO IL=IL1,IL2
        IF(CSZROW(IL).GT.0. .AND. NAGE.EQ.1) ATAU(IL) = 1.*YSPHR
      ENDDO
      DO 412 JK=1,ILEV
       DO 410 IL=IL1,IL2
        IF(CSZROW(IL).GT.0.) THEN
         ZXTP1SO2=XROW(IL,JK,ISO2)+ZXTE(IL,JK,ISO2)*ZTMST
         IF(ZXTP1SO2.LE.ZMIN) THEN
           ZSO2=0.
         ELSE
           ZTK2=ZK2*(TH(IL,JK)/300.)**(-3.3)
           ZM=ZRHO0(IL,JK)*ZNAMAIR
           ZHIL=ZTK2*ZM/ZK2I
           ZEXP=LOG10(ZHIL)
           ZEXP=1./(1.+ZEXP*ZEXP)
           ZTK23B=ZTK2*ZM/(1.+ZHIL)*ZK2F**ZEXP
           ZSO2=ZXTP1SO2*ZRHXTOC(IL,JK)*ZZOH(IL,JK)*ZTK23B*ZDAYL(IL)
           ZSO2=ZSO2*ZRHCTOX(IL,JK)
           ZSO2=MIN(ZSO2,ZXTP1SO2*PQTMST)
           ZXTE(IL,JK,ISO2)=ZXTE(IL,JK,ISO2)-ZSO2
         ENDIF
         SGPP(IL,JK)=ZSO2
!
         ZXTP1DMS = XROW(IL,JK,IDMS) + ZXTE(IL,JK,IDMS)*ZTMST
         IF (ZXTP1DMS.LE.ZMIN) THEN
           ZDMS=0.
         ELSE
           T=TH(IL,JK)
!          ZTK1=(T*EXP(-234./T)+8.46E-10*EXP(7230./T)+2.68E-10*EXP(7810./T))/(1.04E+11*T+88.1*EXP(7460./T))
!
!         * DMS+OH->SO2+MO2+CH2O (JPL 2010)
!
          ZTK1=1.2E-11*EXP(-280./T)
!
!         * DMS+OH+O2->0.75SO2+0.25MSA+MO2 (JPL 2010)
!
!          ZTKA=8.2E-39*EXP(5376./T)
          ZTKA=EXP(5376./T-87.69668)
          ZTKB=1.05E-05*EXP(3644./T)
          ZTK2=ZTKA*ZZO2(IL,JK)/(1.0+ZTKB*0.2095)
          ZDMS=ZXTP1DMS*ZRHXTOC(IL,JK)*ZZOH(IL,JK)*(ZTK1+ZTK2)*ZDAYL(IL)
          ZDMS=ZDMS*ZRHCTOX(IL,JK)
          IF ( ZDMS > ZXTP1DMS*PQTMST ) THEN
            SCALF=ZXTP1DMS*PQTMST/ZDMS
            ZDMS=SCALF*ZDMS
          ENDIF
          ZXTE(IL,JK,IDMS)=ZXTE(IL,JK,IDMS)-ZDMS
          ZXTE(IL,JK,ISO2)=ZXTE(IL,JK,ISO2)+0.75*ZDMS
          SGPP(IL,JK)=SGPP(IL,JK)+0.25*ZDMS
         ENDIF
!
         FAC=DSHJ(IL,JK)*PRESSG(IL)/GRAV
         DOX4ROW(IL)=DOX4ROW(IL)+FAC*ZSO2
         DOXDROW(IL)=DOXDROW(IL)+FAC*ZDMS
        ENDIF
  410  CONTINUE
  412 CONTINUE
!
!     * NIGHT-TIME CHEMISTRY.
!
      DO IL=IL1,IL2
        IF(CSZROW(IL).LE.0. .AND. NAGE.EQ.1) ATAU(IL) = 24.*YSPHR
      ENDDO
      DO 416 JK=1,ILEV
       DO 414 IL=IL1,IL2
        IF(CSZROW(IL).LE.0.) THEN
         ZXTP1DMS=XROW(IL,JK,IDMS)+ ZXTE(IL,JK,IDMS)*ZTMST
         IF(ZXTP1DMS.LE.ZMIN) THEN
           ZDMS=0.
         ELSE
!
! DMS + NO3 -> SO2 + HNO3 + MO2 + CH2O (JPL 2010)
!
!           ZTK3=ZK3*EXP(520./TH(IL,JK))
           ZTK3=1.9E-13*EXP(530./TH(IL,JK))
           ZDMS=ZXTP1DMS*ZRHXTOC(IL,JK)*ZZNO3(IL,JK)*ZTK3
           ZDMS=ZDMS*ZRHCTOX(IL,JK)
           ZDMS=MIN(ZDMS,ZXTP1DMS*PQTMST)
           ZXTE(IL,JK,IDMS)=ZXTE(IL,JK,IDMS)-ZDMS
           ZXTE(IL,JK,ISO2)=ZXTE(IL,JK,ISO2)+ZDMS
           FAC=DSHJ(IL,JK)*PRESSG(IL)/GRAV
           NOXDROW(IL)=NOXDROW(IL)+ZDMS*FAC
         ENDIF
        ENDIF
  414  CONTINUE
  416 CONTINUE
!
!     * FRACTION OF TRACERS IN CLOUDY PART OF THE GRID CELL.
!
      DO 422 JT=1,NTRAC
       DO 421 JK=1,ILEV
        DO 420 IL=IL1,IL2
          IF ( JT.LT.IAIND(1) .OR. JT.GT.IAIND(NTRACP) ) THEN
            XROW(IL,JK,JT)=XROW(IL,JK,JT)+ZXTE(IL,JK,JT)*ZTMST
            XROW(IL,JK,JT)=MAX(XROW(IL,JK,JT),ZERO)
            IF ( JT /= ISO2 .AND. JT /= IHPO ) THEN
              IF ( ITRWET(JT).NE.0 .AND. XROW(IL,JK,JT) > ZMIN .AND. ZCLF(IL,JK) > 1.E-02 ) THEN
                SFRC(IL,JK,JT)=0.
                IF ( ZCLF(IL,JK) < ZCTHR .AND. (1.-ZCLF(IL,JK)) < ZCTHR ) THEN
                  SFRC(IL,JK,JT)=ZCLF(IL,JK)*(1.-ZCLF(IL,JK))*(XCLR(IL,JK,JT)-XCLD(IL,JK,JT))
                ENDIF
              ELSE
                SFRC(IL,JK,JT)=0.
              ENDIF
            ENDIF
          ENDIF
 420    CONTINUE
 421   CONTINUE
 422  CONTINUE
!
      RETURN
      END
