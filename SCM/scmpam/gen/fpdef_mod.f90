!> \file
!> \brief Integer kind types
!
!-----------------------------------------------------------------------
module fpdef
  !
  integer, parameter :: r8 = selected_real_kind(12) ! 8 byte real
  integer, parameter :: r4 = selected_real_kind( 6) ! 4 byte real
  logical :: kdbl !<
  !
end module fpdef
