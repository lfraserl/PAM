subroutine initio(ktime1,rdinput)
  !
  use iodat
  !
  implicit none
!
  integer :: ichk
  integer :: idim
  integer :: is
  integer :: it
  integer :: itx
  integer :: nd
  !
  logical, intent(in) :: ktime1 !<
  logical, intent(in) :: rdinput !<
  !
  !     * initializations.
  !
  do it=1,imaxl
    cna(it:it) = '*'
  end do
  do it=1,itot
    ivar(it)%name=cna
    ivar(it)%lname=cna
    ivar(it)%units=cna
    ivar(it)%dim=cna
    ivarv(it)%fld0d=-ynax
  end do
  do it=1,itot
    ovar(it)%name=cna
    ovar(it)%lname=cna
    ovar(it)%units=cna
    ovar(it)%dim=cna
    ovarv(it)%fld0d=-ynax
  end do
  do it=1,itot
    rvar(it)%name=cna
    rvar(it)%lname=cna
    rvar(it)%units=cna
    rvar(it)%dim=cna
    rvarv(it)%fld0d=-ynax
  end do
  !
  !     * open and read namelists.
  !
  open(20,file='OUTPUT')
  open(30,file='RESTART')
  read(20,nml=ovarnml)
  read(30,nml=rvarnml)
  !
  if (rdinput) then
    open(10,file='INPUT')
    read(10,nml=ivarnml)
  end if
  !
  !     * save dimensions and allocate memory for i/o arrays.
  !
  do it=1,itot
    if (ivar(it)%name /= cna) then
      if ( .not.(ivar(it)%lname /= cna &
          .and. ivar(it)%units /= cna) ) then
        print*,'MISSING LNAME OR UNITS FOR INPUT VARIABLE ', &
                    trim(ivar(it)%name)
        call xit('INITIO',-1)
      end if
      do itx=1,it-1
        if (trim(ivar(it)%name) == trim(ivar(itx)%name) ) then
          print*,'MULTIPLE OCCURRENCES OF INPUT VARIABLE ', &
                      trim(ivar(it)%name)
          call xit('INITIO',-2)
        end if
      end do
      idim=0
      do is=1,ismx
        ctmp=ivar(it)%dim(is)
        if (ctmp /= cna) then
          idim=idim+1
          ichk=0
          do nd=1,ndim
            if (trim(ctmp) == trim(dim(nd)%name) ) then
              dim(nd)%flgi=1
              idima(idim)=dim(nd)%size
              if (dim(nd)%size == inax) then
                print*,'DIMENSION ',trim(dim(nd)%name),' UNDEFINED'
                call xit ('INITIO',-3)
              end if
              ichk=1
            end if
          end do
          if (ichk == 0) then
            print*,'INCORRECT DIMENSION FOR INPUT VARIABLE ', &
                       trim(ivar(it)%name)
            call xit('INITIO',-4)
          end if
        end if
      end do
      if (idim == 0) then
        ivarv(it)%fld0d=ynax
      else if (idim == 1) then
        allocate(ivarv(it)%fld1d(idima(1)))
        ivarv(it)%fld1d(:)=ynax
      else if (idim == 2) then
        allocate(ivarv(it)%fld2d(idima(1),idima(2)))
        ivarv(it)%fld2d(:,:)=ynax
      else if (idim == 3) then
        allocate(ivarv(it)%fld3d(idima(1),idima(2),idima(3)))
        ivarv(it)%fld3d(:,:,:)=ynax
      else if (idim == 4) then
        allocate(ivarv(it)%fld4d(idima(1),idima(2),idima(3), &
                                                            idima(4)))
        ivarv(it)%fld4d(:,:,:,:)=ynax
      else
        call xit('INITIO',-5)
      end if
    end if
  end do
  do it=1,itot
    if (ovar(it)%name /= cna) then
      if ( .not.(ovar(it)%lname /= cna &
          .and. ovar(it)%units /= cna) ) then
        print*,'MISSING LNAME OR UNITS FOR OUTPUT VARIABLE ', &
                    trim(ovar(it)%name)
        call xit('INITIO',-6)
      end if
      do itx=1,it-1
        if (trim(ovar(it)%name) == trim(ovar(itx)%name) ) then
          print*,'MULTIPLE OCCURRENCES OF OUTPUT VARIABLE ', &
                      trim(ovar(it)%name)
          call xit('INITIO',-7)
        end if
      end do
      idim=0
      do is=1,ismx
        ctmp=ovar(it)%dim(is)
        if (ktime1 .and. trim(ctmp) == 'time' ) then
          ovar(it)%dim(is)='time1'
          ctmp=ovar(it)%dim(is)
        end if
        if (ctmp /= cna) then
          idim=idim+1
          ichk=0
          do nd=1,ndim
            if (trim(ctmp) == trim(dim(nd)%name) ) then
              dim(nd)%flgo=1
              idima(idim)=dim(nd)%size
              if (dim(nd)%size == inax) then
                print*,'DIMENSION ',trim(dim(nd)%name),' UNDEFINED'
                call xit ('INITIO',-8)
              end if
              ichk=1
            end if
          end do
          if (ichk == 0) then
            print*,'INCORRECT DIMENSION FOR OUTPUT VARIABLE ', &
                       trim(ovar(it)%name)
            call xit('INITIO',-9)
          end if
        end if
      end do
      if (idim == 0) then
        ovarv(it)%fld0d=ynax
      else if (idim == 1) then
        allocate(ovarv(it)%fld1d(idima(1)))
        ovarv(it)%fld1d(:)=ynax
      else if (idim == 2) then
        allocate(ovarv(it)%fld2d(idima(1),idima(2)))
        ovarv(it)%fld2d(:,:)=ynax
      else if (idim == 3) then
        allocate(ovarv(it)%fld3d(idima(1),idima(2),idima(3)))
        ovarv(it)%fld3d(:,:,:)=ynax
      else if (idim == 4) then
        allocate(ovarv(it)%fld4d(idima(1),idima(2),idima(3), &
                                                            idima(4)))
        ovarv(it)%fld4d(:,:,:,:)=ynax
      else
        call xit('INITIO',-10)
      end if
    end if
  end do
  do it=1,itot
    if (rvar(it)%name /= cna) then
      if ( .not.(rvar(it)%lname /= cna &
          .and. rvar(it)%units /= cna) ) then
        print*,'MISSING LNAME OR UNITS FOR RESTART VARIABLE ', &
                    trim(rvar(it)%name)
        call xit('INITIO',-11)
      end if
      do itx=1,it-1
        if (trim(rvar(it)%name) == trim(rvar(itx)%name) ) then
          print*,'MULTIPLE OCCURRENCES OF RESTART VARIABLE ', &
                      trim(rvar(it)%name)
          call xit('INITIO',-12)
        end if
      end do
      idim=0
      do is=1,ismx
        ctmp=rvar(it)%dim(is)
        if (ctmp /= cna) then
          idim=idim+1
          ichk=0
          do nd=1,ndim
            if (trim(ctmp) == trim(dim(nd)%name) ) then
              dim(nd)%flgrs=1
              idima(idim)=dim(nd)%size
              if (dim(nd)%size == inax) then
                print*,'DIMENSION ',trim(dim(nd)%name),' UNDEFINED'
                call xit ('INITIO',-13)
              end if
              ichk=1
            end if
          end do
          if (ichk == 0) then
            print*,'INCORRECT DIMENSION FOR RESTART VARIABLE ', &
                       trim(rvar(it)%name)
            call xit('INITIO',-14)
          end if
        end if
      end do
      if (idim == 0) then
        rvarv(it)%fld0d=ynax
      else if (idim == 1) then
        allocate(rvarv(it)%fld1d(idima(1)))
        rvarv(it)%fld1d(:)=ynax
      else if (idim == 2) then
        allocate(rvarv(it)%fld2d(idima(1),idima(2)))
        rvarv(it)%fld2d(:,:)=ynax
      else if (idim == 3) then
        allocate(rvarv(it)%fld3d(idima(1),idima(2),idima(3)))
        rvarv(it)%fld3d(:,:,:)=ynax
      else if (idim == 4) then
        allocate(rvarv(it)%fld4d(idima(1),idima(2),idima(3), &
                                                             idima(4)))
        rvarv(it)%fld4d(:,:,:,:)=ynax
      else
        call xit('INITIO',-15)
      end if
    end if
  end do
  !
  !     * close input files.
  !
  close(11)
  close(12)
  if (rdinput) then
    close(10)
  end if
  !
end subroutine initio
