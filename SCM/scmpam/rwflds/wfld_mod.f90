module wfld

contains

  subroutine w0dfld(cname,avar)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in) :: avar !<
    !
    ichck=0
    do it=1,itot
      if (abs(ovarv(it)%fld0d+ynax) > ysmallx &
          .and. trim(ovar(it)%name) == trim(cname) ) then
        ichck=1
        ovarv(it)%fld0d=avar
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
      call xit('W0DFLD',-1)
    end if
    !
  end subroutine w0dfld
  subroutine w0dfldrs(cname,avar)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in) :: avar !<
    !
    ichck=0
    do it=1,itot
      if (abs(rvarv(it)%fld0d+ynax) > ysmallx &
          .and. trim(rvar(it)%name) == trim(cname) ) then
        ichck=1
        rvarv(it)%fld0d=avar
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname), &
               ' IN LIST OF RESTART VARIABLES'
      call xit('W0DFLDRS',-1)
    end if
    !
  end subroutine w0dfldrs
  subroutine w1dfld(cname,avar,idim1,j1,j2)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in), dimension(idim1) :: avar !<
    integer, intent(in) :: idim1 !<
    integer, intent(in), optional :: j1 !<
    integer, intent(in), optional :: j2 !<
    integer :: j1t !<
    integer :: j2t !<
    integer, dimension(1) :: ishape !<
    !
    if (present(j1) .and. present(j2) ) then
      if (j1 < 1 .or. j2 > idim1) call xit('W1DFLD',-1)
      j1t=j1
      j2t=j2
    else
      j1t=1
      j2t=idim1
    end if
    !
    ichck=0
    do it=1,itot
      if (allocated(ovarv(it)%fld1d) &
          .and. trim(ovar(it)%name) == trim(cname) ) then
        ichck=1
        !
        ishape=shape(ovarv(it)%fld1d)
        if (ishape(1) == idim1) then
          ovarv(it)%fld1d(j1t:j2t)=avar(j1t:j2t)
        else
          print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
          call xit('W1DFLD',-2)
        end if
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
      call xit('W1DFLD',-3)
    end if
    !      print '("W1DFLD: ", a10, "   [",i2,"]  [",i2,"-",i2,"]", es12.2)', CNAME,IDIM1,J1T,J2T, sum(avar)/size(avar)
    !
  end subroutine w1dfld
  subroutine w1dfldrs(cname,avar,idim1,j1,j2)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in), dimension(idim1) :: avar !<
    integer, intent(in) :: idim1 !<
    integer, intent(in), optional :: j1 !<
    integer, intent(in), optional :: j2 !<
    integer :: j1t !<
    integer :: j2t !<
    integer, dimension(1) :: ishape !<
    !
    if (present(j1) .and. present(j2) ) then
      if (j1 < 1 .or. j2 > idim1) call xit('W1DFLDRS',-1)
      j1t=j1
      j2t=j2
    else
      j1t=1
      j2t=idim1
    end if
    !
    ichck=0
    do it=1,itot
      if (allocated(rvarv(it)%fld1d) &
          .and. trim(rvar(it)%name) == trim(cname) ) then
        ichck=1
        !
        ishape=shape(rvarv(it)%fld1d)
        if (ishape(1) == idim1) then
          rvarv(it)%fld1d(j1t:j2t)=avar(j1t:j2t)
        else
          print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
          call xit('W1DFLDRS',-2)
        end if
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname), &
               ' IN LIST OF RESTART VARIABLES'
      call xit('W1DFLDRS',-3)
    end if
    !
  end subroutine w1dfldrs
  subroutine w2dfld(cname,avar,idim1,idim2,j1,j2,k1,k2)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in), dimension(idim1,idim2) :: avar !<
    integer, intent(in) :: idim1 !<
    integer, intent(in) :: idim2 !<
    integer, intent(in), optional :: j1 !<
    integer, intent(in), optional :: j2 !<
    integer, intent(in), optional :: k1 !<
    integer, intent(in), optional :: k2 !<
    integer :: j1t !<
    integer :: j2t !<
    integer :: k1t !<
    integer :: k2t !<
    integer, dimension(2) :: ishape !<
    !
    if (present(j1) .and. present(j2) &
        .and. present(k1) .and. present(k2) ) then
      if (j1 < 1 .or. k1 < 1 .or. j2 > idim1 .or. k2 > idim2) &
          call xit('W2DFLD',-1)
      j1t=j1
      j2t=j2
      k1t=k1
      k2t=k2
    else
      j1t=1
      j2t=idim1
      k1t=1
      k2t=idim2
    end if
    !
    ichck=0
    do it=1,itot
      if (allocated(ovarv(it)%fld2d) &
          .and. trim(ovar(it)%name) == trim(cname) ) then
        ichck=1
        !
        ishape=shape(ovarv(it)%fld2d)
        if (ishape(1) == idim1 .and. ishape(2) == idim2) then
          ovarv(it)%fld2d(j1t:j2t,k1t:k2t)=avar(j1t:j2t,k1t:k2t)
        else if (ishape(1) == idim2 .and. ishape(2) == idim1) then
          do i1=k1t,k2t
            do i2=j1t,j2t
              ovarv(it)%fld2d(i1,i2)=avar(i2,i1)
            end do
          end do
        else
          print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
          call xit('W2DFLD',-2)
        end if
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
      call xit('W2DFLD',-3)
    end if
    !      print '("W2DFLD: ", a10, "   [",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', &
    !       cname, idim1,idim2, j1t,j2t, k1t,k2t, sum(avar)/size(avar)
    !
  end subroutine w2dfld
  subroutine w2dfldrs(cname,avar,idim1,idim2,j1,j2,k1,k2)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in), dimension(idim1,idim2) :: avar !<
    integer, intent(in) :: idim1 !<
    integer, intent(in) :: idim2 !<
    integer, intent(in), optional :: j1 !<
    integer, intent(in), optional :: j2 !<
    integer, intent(in), optional :: k1 !<
    integer, intent(in), optional :: k2 !<
    integer :: j1t !<
    integer :: j2t !<
    integer :: k1t !<
    integer :: k2t !<
    integer, dimension(2) :: ishape !<
    !
    if (present(j1) .and. present(j2) &
        .and. present(k1) .and. present(k2) ) then
      if (j1 < 1 .or. k1 < 1 .or. j2 > idim1 .or. k2 > idim2) &
          call xit('W2DFLDRS',-1)
      j1t=j1
      j2t=j2
      k1t=k1
      k2t=k2
    else
      j1t=1
      j2t=idim1
      k1t=1
      k2t=idim2
    end if
    !
    ichck=0
    do it=1,itot
      if (allocated(rvarv(it)%fld2d) &
          .and. trim(rvar(it)%name) == trim(cname) ) then
        ichck=1
        !
        ishape=shape(rvarv(it)%fld2d)
        if (ishape(1) == idim1 .and. ishape(2) == idim2) then
          rvarv(it)%fld2d(j1t:j2t,k1t:k2t)=avar(j1t:j2t,k1t:k2t)
        else if (ishape(1) == idim2 .and. ishape(2) == idim1) then
          do i1=k1t,k2t
            do i2=j1t,j2t
              rvarv(it)%fld2d(i1,i2)=avar(i2,i1)
            end do
          end do
        else
          print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
          call xit('W2DFLDRS',-2)
        end if
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname), &
               ' IN LIST OF RESTART VARIABLES'
      call xit('W2DFLDRS',-3)
    end if
    !
  end subroutine w2dfldrs
  subroutine w3dfld(cname,avar,idim1,idim2,idim3, &
                        j1,j2,k1,k2,l1,l2)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in), dimension(idim1,idim2,idim3) :: avar !<
    integer, intent(in), optional :: j1 !<
    integer, intent(in), optional :: j2 !<
    integer, intent(in), optional :: k1 !<
    integer, intent(in), optional :: k2 !<
    integer, intent(in), optional :: l1 !<
    integer, intent(in), optional :: l2 !<
    integer, intent(in) :: idim1 !<
    integer, intent(in) :: idim2 !<
    integer, intent(in) :: idim3 !<
    integer :: j1t !<
    integer :: j2t !<
    integer :: k1t !<
    integer :: k2t !<
    integer :: l1t !<
    integer :: l2t !<
    integer, dimension(3) :: ishape !<
    !
    if (present(j1) .and. present(j2) &
        .and. present(k1) .and. present(k2) &
        .and. present(l1) .and. present(l2) ) then
      if (j1 < 1 .or. k1 < 1 .or. l1 < 1 &
          .or. j2 > idim1 .or. k2 > idim2 .or. l2 > idim3) &
          call xit('W3DFLD',-1)
      j1t=j1
      j2t=j2
      k1t=k1
      k2t=k2
      l1t=l1
      l2t=l2
    else
      j1t=1
      j2t=idim1
      k1t=1
      k2t=idim2
      l1t=1
      l2t=idim3
    end if
    !
    ichck=0
    do it=1,itot
      if (allocated(ovarv(it)%fld3d) &
          .and. trim(ovar(it)%name) == trim(cname) ) then
        ichck=1
        !
        ishape=shape(ovarv(it)%fld3d)
        if (ishape(1) == idim1 .and. ishape(2) == idim2 &
            .and. ishape(3) == idim3) then
          ovarv(it)%fld3d(j1t:j2t,k1t:k2t,l1t:l2t)= &
                                                      avar(j1t:j2t,k1t:k2t,l1t:l2t)
        else if (ishape(1) == idim1 .and. ishape(2) == idim3 &
               .and. ishape(3) == idim2) then
          do i2=k1t,k2t
            do i3=l1t,l2t
              ovarv(it)%fld3d(j1t:j2t,i3,i2)=avar(j1t:j2t,i2,i3)
            end do
          end do
        else if (ishape(1) == idim2 .and. ishape(2) == idim1 &
               .and. ishape(3) == idim3) then
          do i1=j1t,j2t
            do i2=k1t,k2t
              ovarv(it)%fld3d(i2,i1,l1t:l2t)=avar(i1,i2,l1t:l2t)
            end do
          end do
        else if (ishape(1) == idim2 .and. ishape(2) == idim3 &
               .and. ishape(3) == idim1) then
          do i1=j1t,j2t
            do i2=k1t,k2t
              do i3=l1t,l2t
                ovarv(it)%fld3d(i2,i3,i1)=avar(i1,i2,i3)
              end do
            end do
          end do
        else if (ishape(1) == idim3 .and. ishape(2) == idim1 &
               .and. ishape(3) == idim2) then
          do i1=j1t,j2t
            do i2=k1t,k2t
              do i3=l1t,l2t
                ovarv(it)%fld3d(i3,i1,i2)=avar(i1,i2,i3)
              end do
            end do
          end do
        else if (ishape(1) == idim3 .and. ishape(2) == idim2 &
               .and. ishape(3) == idim1) then
          do i1=j1t,j2t
            do i3=l1t,l2t
              ovarv(it)%fld3d(i3,k1t:k2t,i1)=avar(i1,k1t:k2t,i3)
            end do
          end do
        else
          print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
          call xit('W3DFLD',-2)
        end if
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
      call xit('W3DFLD',-3)
    end if
    !      print '("W3DFLD: ", a10, "   [",i2,"x",i2,"x",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]  [",i2,"-",i2,"]", es12.2)', &
    !       cname,idim1,idim2,idim3,j1t,j2t,k1t,k2t,l1t,l2t, sum(avar)/size(avar)
    !
  end subroutine w3dfld
  subroutine w3dfldrs(cname,avar,idim1,idim2,idim3, &
                          j1,j2,k1,k2,l1,l2)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in), dimension(idim1,idim2,idim3) :: avar !<
    integer, intent(in) :: idim1 !<
    integer, intent(in) :: idim2 !<
    integer, intent(in) :: idim3 !<
    integer, intent(in), optional :: j1 !<
    integer, intent(in), optional :: j2 !<
    integer, intent(in), optional :: k1 !<
    integer, intent(in), optional :: k2 !<
    integer, intent(in), optional :: l1 !<
    integer, intent(in), optional :: l2 !<
    integer :: j1t !<
    integer :: j2t !<
    integer :: k1t !<
    integer :: k2t !<
    integer :: l1t !<
    integer :: l2t !<
    integer, dimension(3) :: ishape !<
    !
    if (present(j1) .and. present(j2) &
        .and. present(k1) .and. present(k2) &
        .and. present(l1) .and. present(l2) ) then
      if (j1 < 1 .or. k1 < 1 .or. l1 < 1 &
          .or. j2 > idim1 .or. k2 > idim2 .or. l2 > idim3) &
          call xit('W3DFLDRS',-1)
      j1t=j1
      j2t=j2
      k1t=k1
      k2t=k2
      l1t=l1
      l2t=l2
    else
      j1t=1
      j2t=idim1
      k1t=1
      k2t=idim2
      l1t=1
      l2t=idim3
    end if
    !
    ichck=0
    do it=1,itot
      if (allocated(rvarv(it)%fld3d) &
          .and. trim(rvar(it)%name) == trim(cname) ) then
        ichck=1
        !
        ishape=shape(rvarv(it)%fld3d)
        if (ishape(1) == idim1 .and. ishape(2) == idim2 &
            .and. ishape(3) == idim3) then
          rvarv(it)%fld3d(j1t:j2t,k1t:k2t,l1t:l2t)= &
                                                      avar(j1t:j2t,k1t:k2t,l1t:l2t)
        else if (ishape(1) == idim1 .and. ishape(2) == idim3 &
               .and. ishape(3) == idim2) then
          do i2=k1t,k2t
            do i3=l1t,l2t
              rvarv(it)%fld3d(j1t:j2t,i3,i2)=avar(j1t:j2t,i2,i3)
            end do
          end do
        else if (ishape(1) == idim2 .and. ishape(2) == idim1 &
               .and. ishape(3) == idim3) then
          do i1=j1t,j2t
            do i2=k1t,k2t
              rvarv(it)%fld3d(i2,i1,l1t:l2t)=avar(i1,i2,l1t:l2t)
            end do
          end do
        else if (ishape(1) == idim2 .and. ishape(2) == idim3 &
               .and. ishape(3) == idim1) then
          do i1=j1t,j2t
            do i2=k1t,k2t
              do i3=l1t,l2t
                rvarv(it)%fld3d(i2,i3,i1)=avar(i1,i2,i3)
              end do
            end do
          end do
        else if (ishape(1) == idim3 .and. ishape(2) == idim1 &
               .and. ishape(3) == idim2) then
          do i1=j1t,j2t
            do i2=k1t,k2t
              do i3=l1t,l2t
                rvarv(it)%fld3d(i3,i1,i2)=avar(i1,i2,i3)
              end do
            end do
          end do
        else if (ishape(1) == idim3 .and. ishape(2) == idim2 &
               .and. ishape(3) == idim1) then
          do i1=j1t,j2t
            do i3=l1t,l2t
              rvarv(it)%fld3d(i3,k1t:k2t,i1)=avar(i1,k1t:k2t,i3)
            end do
          end do
        else
          print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
          call xit('W3DFLDRS',-2)
        end if
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname), &
               ' IN LIST OF RESTART VARIABLES'
      call xit('W3DFLDRS',-3)
    end if
    !
  end subroutine w3dfldrs
  subroutine w4dfld(cname,avar,idim1,idim2,idim3,idim4, &
                        j1,j2,k1,k2,l1,l2,m1,m2)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in), dimension(idim1,idim2,idim3,idim4) :: avar !<
    integer, intent(in) :: idim1 !<
    integer, intent(in) :: idim2 !<
    integer, intent(in) :: idim3 !<
    integer, intent(in) :: idim4 !<
    integer, intent(in), optional :: j1 !<
    integer, intent(in), optional :: j2 !<
    integer, intent(in), optional :: k1 !<
    integer, intent(in), optional :: k2 !<
    integer, intent(in), optional :: l1 !<
    integer, intent(in), optional :: l2 !<
    integer, intent(in), optional :: m1 !<
    integer, intent(in), optional :: m2 !<
    integer :: j1t !<
    integer :: j2t !<
    integer :: k1t !<
    integer :: k2t !<
    integer :: l1t !<
    integer :: l2t !<
    integer :: m1t !<
    integer :: m2t !<
    integer, dimension(4) :: ishape !<
    !
    if (present(j1) .and. present(j2) &
        .and. present(k1) .and. present(k2) &
        .and. present(l1) .and. present(l2) &
        .and. present(m1) .and. present(m2) ) then
      if (j1 < 1 .or. k1 < 1 .or. l1 < 1 .or. m1 < 1 &
          .or. j2 > idim1 .or. k2 > idim2 .or. l2 > idim3 &
          .or. m2 > idim4) &
          call xit('W4DFLD',-1)
      j1t=j1
      j2t=j2
      k1t=k1
      k2t=k2
      l1t=l1
      l2t=l2
      m1t=m1
      m2t=m2
    else
      j1t=1
      j2t=idim1
      k1t=1
      k2t=idim2
      l1t=1
      l2t=idim3
      m1t=1
      m2t=idim4
    end if
    !
    ichck=0
    do it=1,itot
      if (allocated(ovarv(it)%fld4d) &
          .and. trim(ovar(it)%name) == trim(cname) ) then
        ichck=1
        !
        ishape=shape(ovarv(it)%fld4d)
        if (ishape(1) == idim1 .and. ishape(2) == idim2 &
            .and. ishape(3) == idim3 .and. ishape(4) == idim4) then
          ovarv(it)%fld4d(j1t:j2t,k1t:k2t,l1t:l2t,m1t:m2t)= &
                                                              avar(j1t:j2t,k1t:k2t,l1t:l2t,m1t:m2t)
        else
          print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
          call xit('W4DFLD',-2)
        end if
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname),' IN LIST OF IO VARIABLES'
      call xit('W4DFLD',-3)
    end if
    !
  end subroutine w4dfld
  subroutine w4dfldrs(cname,avar,idim1,idim2,idim3,idim4, &
                          j1,j2,k1,k2,l1,l2,m1,m2)
    !
    use iodat
    !
    implicit real (a-h,o-z),integer (i-n)
    !
    character(len=*), intent(in) :: cname !<
    real, intent(in), dimension(idim1,idim2,idim3,idim4) :: avar !<
    integer, intent(in) :: idim1 !<
    integer, intent(in) :: idim2 !<
    integer, intent(in) :: idim3 !<
    integer, intent(in) :: idim4 !<
    integer, intent(in), optional :: j1 !<
    integer, intent(in), optional :: j2 !<
    integer, intent(in), optional :: k1 !<
    integer, intent(in), optional :: k2 !<
    integer, intent(in), optional :: l1 !<
    integer, intent(in), optional :: l2 !<
    integer, intent(in), optional :: m1 !<
    integer, intent(in), optional :: m2 !<
    integer :: j1t !<
    integer :: j2t !<
    integer :: k1t !<
    integer :: k2t !<
    integer :: l1t !<
    integer :: l2t !<
    integer :: m1t !<
    integer :: m2t !<
    integer, dimension(4) :: ishape !<
    !
    if (present(j1) .and. present(j2) &
        .and. present(k1) .and. present(k2) &
        .and. present(l1) .and. present(l2) &
        .and. present(m1) .and. present(m2) ) then
      if (j1 < 1 .or. k1 < 1 .or. l1 < 1 .or. m1 < 1 &
          .or. j2 > idim1 .or. k2 > idim2 .or. l2 > idim3 &
          .or. m2 > idim4) &
          call xit('W4DFLDRS',-1)
      j1t=j1
      j2t=j2
      k1t=k1
      k2t=k2
      l1t=l1
      l2t=l2
      m1t=m1
      m2t=m2
    else
      j1t=1
      j2t=idim1
      k1t=1
      k2t=idim2
      l1t=1
      l2t=idim3
      m1t=1
      m2t=idim4
    end if
    !
    ichck=0
    do it=1,itot
      if (allocated(rvarv(it)%fld4d) &
          .and. trim(rvar(it)%name) == trim(cname) ) then
        ichck=1
        !
        ishape=shape(rvarv(it)%fld4d)
        if (ishape(1) == idim1 .and. ishape(2) == idim2 &
            .and. ishape(3) == idim3 .and. ishape(4) == idim4) then
          rvarv(it)%fld4d(j1t:j2t,k1t:k2t,l1t:l2t,m1t:m2t)= &
                                                              avar(j1t:j2t,k1t:k2t,l1t:l2t,m1t:m2t)
        else
          print*,'INCOMPATIBLE DIMENSIONS VARIABLE ',trim(cname)
          call xit('W4DFLDRS',-2)
        end if
      end if
    end do
    if (ichck==0) then
      print*,'NO VARIABLE ',trim(cname), &
               ' IN LIST OF RESTART VARIABLES'
      call xit('W4DFLDRS',-3)
    end if
    !
  end subroutine w4dfldrs

end module wfld
