subroutine wetdep4(ilg,il1,il2,ilev,ztmst, pcons2, pxtp10, &
                         pxtp1c,dshj,shj,pressg,th,qv,pmratep,zmlwc, &
                         pfsnow,pfrain,pdep3d,zclf,clrfr,phenry, &
                         clrfs,pfevap,pfsubl,pdclr,pdcld,jt,iso4)
  !
  !     *wetdep4* calculates the wet deposition of trace gases or aerosols
  !
  !     * feb 18/2009 - k.vonsalzen. new version for gcm15h:
  !     *                            - add diagnostics for in-cloud and
  !     *                              clear-sky sources and sinks of
  !     *                              tracers.
  !     * dec 16/07 -  k. vonsalzen. previous version wetdep3 for gcm15g:
  !     *                            - bugfixes for scavenging.
  !     * jun 20/06 -  m. lazare.    previous version wetdep2 for gcm15f:
  !     *                            - use variable instead of constant
  !     *                              in intrinsics such as "MAX",
  !     *                              so that can compile in 32-bit mode
  !     *                              with real*8.
  !
  !   p rpose
  !  --------- &
  !   t  calculate the wet scavenging of gases or aerosols in clouds
  !
  !   i terface
  !  ------------- &
  !   t is routine is called from *xtchemie*
  !   c
  !
  use sdphys
  !
  implicit none
  integer :: il
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer, intent(in) :: iso4
  integer :: jk
  integer, intent(in) :: jt
  real :: one
  real, intent(in) :: pcons2
  real :: pqtmst
  real :: vtmpc1
  real :: zbcscav
  real :: zcolleff
  real :: zcons1
  real :: zep
  real :: zeps
  real :: zero
  real :: zftom
  real :: zicscav
  real :: zmin
  real :: zmtof
  real :: zrho0
  real, intent(in) :: ztmst
  real :: zzeff
  !
  real, intent(in)   :: zmlwc(ilg,ilev) !<
  real, intent(inout)   :: pxtp10(ilg,ilev) !<
  real, intent(inout)   :: pxtp1c(ilg,ilev) !<
  real, intent(in)   :: dshj(ilg,ilev) !<
  real, intent(in)   :: shj(ilg,ilev) !<
  real, intent(in)   :: pmratep(ilg,ilev) !<
  real, intent(in)   :: pfsnow(ilg,ilev) !<
  real, intent(in)   :: th(ilg,ilev) !<
  real, intent(in)   :: pfevap(ilg,ilev) !<
  real, intent(in)   :: zclf(ilg,ilev) !<
  real, intent(in)   :: clrfr(ilg,ilev) !<
  real, intent(in)   :: pressg(ilg) !<
  real, intent(in)   :: qv(ilg,ilev) !<
  real, intent(in)   :: pfrain(ilg,ilev) !<
  real, intent(in)   :: clrfs(ilg,ilev) !<
  real, intent(in)   :: pfsubl(ilg,ilev) !<
  real, intent(inout)   :: pdep3d(ilg,ilev) !<
  real, intent(in)   :: phenry(ilg,ilev) !<
  real   :: zdepr(ilg) !<
  real   :: zdeps(ilg) !<
  real   :: pdepr(ilg,ilev) !<
  real   :: pdeps(ilg,ilev) !<
  real, intent(inout)   :: pdclr(ilg,ilev) !<
  real, intent(inout)   :: pdcld(ilg,ilev) !<
  !
  data zero,one /0., 1./
  !====================================================================
  !     * physical constants
  !
  pqtmst=1./ztmst
  zcons1=5.2
  zcolleff=0.1
  zmin=1.e-20
  zep=1.
  vtmpc1=rgasv/rgas-1.
  !
  pdeps=0.
  pdepr=0.
  pdclr=0.
  pdcld=0.
  do il=il1,il2
    zdepr(il)=0.
    zdeps(il)=0.
  end do ! loop 100
  !
  !     * major vertical loop.
  !
  do jk=1,ilev
    do il=il1,il2
      pxtp1c(il,jk)=max(zero,pxtp1c(il,jk))
      pxtp10(il,jk)=max(zero,pxtp10(il,jk))
    end do ! loop 110
    !
    !   2  in-cloud scavenging (giorgi + chameides)
    !
    do il=il1,il2
      if (pmratep(il,jk)>zmin) then
        zicscav=pmratep(il,jk)/zmlwc(il,jk)
        zicscav=max(zero,min(one,zicscav))
        zeps=zep*phenry(il,jk)
        zicscav=zeps*zicscav
        !
        !           no scavenging in ice-clouds
        !
        pdepr(il,jk)=pdepr(il,jk)+pxtp1c(il,jk)*zicscav &
                   *zclf(il,jk)
        pdcld(il,jk)=pdcld(il,jk)+pxtp1c(il,jk)*zicscav
        !           pxtp1c(il,jk)=pxtp1c(il,jk)*(1.-zicscav)
      end if
    end do ! loop 120
    !
    !   3  below cloud scavenging for ice and water clouds.
    !
    do il=il1,il2
      if (pfsnow(il,jk)>zmin.and.clrfs(il,jk)>=0.05) then
        zzeff=zcolleff*3.
        zftom=1./(dshj(il,jk)*pressg(il)*pcons2)
        zrho0=pressg(il)*shj(il,jk)/(rgas*th(il,jk) &
                     *(1.+vtmpc1*qv(il,jk)))
        zbcscav=zcons1*zzeff*(pfsnow(il,jk)/clrfs(il,jk)) &
                   *zftom*zrho0
        zbcscav=max(zero,min(one,zbcscav))
        pdeps(il,jk)=pdeps(il,jk)+zbcscav*pxtp10(il,jk)* &
                         clrfs(il,jk)
        if (zclf(il,jk) < 0.99) then
          pdclr(il,jk)=pdclr(il,jk)+zbcscav*pxtp10(il,jk) &
                          *clrfs(il,jk)/(1.-zclf(il,jk))
        end if
      end if
    end do ! loop 140
    do il=il1,il2
      if (pfrain(il,jk)>zmin.and.clrfr(il,jk)>=0.05) then
        zzeff=zcolleff*3.
        zftom=1./(dshj(il,jk)*pressg(il)*pcons2)
        zrho0=pressg(il)*shj(il,jk)/(rgas*th(il,jk) &
                     *(1.+vtmpc1*qv(il,jk)))
        zbcscav=zcons1*zzeff*(pfrain(il,jk)/clrfr(il,jk)) &
                   *zftom*zrho0
        zbcscav=max(zero,min(one,zbcscav))
        pdepr(il,jk)=pdepr(il,jk)+zbcscav*pxtp10(il,jk)* &
                         clrfr(il,jk)
        if (zclf(il,jk) < 0.99) then
          pdclr(il,jk)=pdclr(il,jk)+zbcscav*pxtp10(il,jk) &
                          *clrfr(il,jk)/(1.-zclf(il,jk))
        end if
      end if
    end do ! loop 145
    do il=il1,il2
      zmtof=dshj(il,jk)*pressg(il)*pcons2
      zdepr(il)=zdepr(il)+pdepr(il,jk)*zmtof
      zdeps(il)=zdeps(il)+pdeps(il,jk)*zmtof
    end do
    !
    !   4  reevaporation. only complete evaporation of rrecip in the
    !      clear-sky portion of the grid cell that is affected by rain)
    !      will cause tracers to be released into the environment.
    !
    do il=il1,il2
      zftom=1./(dshj(il,jk)*pressg(il)*pcons2)
      pdeps(il,jk)=pdeps(il,jk)-zdeps(il)*zftom*pfsubl(il,jk)
      if (zclf(il,jk) < 0.99) then
        pdclr(il,jk)=pdclr(il,jk) &
                        -zdeps(il)*zftom*pfsubl(il,jk)/(1.-zclf(il,jk))
      end if
      zdeps(il)=zdeps(il)*(1.-pfsubl(il,jk))
    end do
    do il=il1,il2
      zftom=1./(dshj(il,jk)*pressg(il)*pcons2)
      pdepr(il,jk)=pdepr(il,jk)-zdepr(il)*zftom*pfevap(il,jk)
      if (zclf(il,jk) < 0.99) then
        pdclr(il,jk)=pdclr(il,jk) &
                        -zdepr(il)*zftom*pfevap(il,jk)/(1.-zclf(il,jk))
      end if
      zdepr(il)=zdepr(il)*(1.-pfevap(il,jk))
    end do
    do il=il1,il2
      pdep3d(il,jk)=pdep3d(il,jk)+pdepr(il,jk)+pdeps(il,jk)
      pdclr (il,jk)=min(pdclr(il,jk),pxtp10(il,jk))
      pdcld (il,jk)=min(pdcld(il,jk),pxtp1c(il,jk))
    end do
  end do ! loop 160
  !
  return
end
