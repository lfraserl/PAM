subroutine wncdat(file_name)
  !
  use iodat
  use netcdf
  !
  implicit none
  !
  integer :: is !<
  integer :: it !<
  integer :: nd !<
  integer :: iv !<
  integer :: ichck !<
  integer :: idim !<

  character(len=*), intent(in) :: file_name !<
  integer :: ncid !<
  integer :: fil_varid !<
  integer, dimension(ndim) :: lvl_dimid !<
  integer, dimension(ndim) :: lvl_varid !<
  integer, dimension(itot) :: var_varid !<
  integer, dimension(1) :: dimd1d !<
  integer, dimension(1) :: cnt1d !<
  integer, dimension(2) :: dimd2d !<
  integer, dimension(2) :: cnt2d !<
  integer, dimension(3) :: dimd3d !<
  integer, dimension(3) :: cnt3d !<
  integer, dimension(4) :: dimd4d !<
  integer, dimension(4) :: cnt4d !<
  !
  !     * create output file.
  !
  call check (nf90_create(trim(file_name)//'.nc', &
                  nf90_clobber, ncid) )
  !
  !     * define the dimensions.
  !
  do nd=1,ndim
    if (dim(nd)%flgo == 1) then
      call check(nf90_def_dim(ncid, trim(dim(nd)%name), &
                     dim(nd)%size, lvl_dimid(nd)) )
    end if
  end do
  !
  !     * define the coordindate variables.
  !
  do nd=1,ndim
    if (dim(nd)%flgo == 1) then
      call check(nf90_def_var(ncid, trim(dim(nd)%name), &
                     nf90_real, lvl_dimid(nd), &
                     lvl_varid(nd)) )
    end if
  end do
  !
  !     * assign attributes to coordinate variables.
  !
  do nd=1,ndim
    if (dim(nd)%flgo == 1) then
      call check(nf90_put_att(ncid, lvl_varid(nd), "units", &
                     trim(dim(nd)%units)) )
      call check(nf90_put_att(ncid, lvl_varid(nd), "long_name", &
                     trim(dim(nd)%lname)) )
    end if
  end do
  !
  !      define the netcdf variables for each field.
  !
  iv=0
  do it=1,itot
    ichck=0
    if (abs(ovarv(it)%fld0d+ynax) > ysmallx) then
      ichck=1
      iv=iv+1
      call check(nf90_def_var(ncid,trim(ovar(it)%name), &
                     nf90_real, var_varid(iv)) )
    else if (allocated(ovarv(it)%fld1d) ) then
      ichck=1
      iv=iv+1
      idim=0
      do is=1,ismx
        ctmp=ovar(it)%dim(is)
        if (ctmp /= cna) then
          idim=idim+1
          do nd=1,ndim
            if (trim(ctmp) == trim(dim(nd)%name) ) then
              dimd1d(idim)=lvl_varid(nd)
            end if
          end do
        end if
      end do
      call check(nf90_def_var(ncid,trim(ovar(it)%name), &
                     nf90_real, dimd1d, var_varid(iv)) )
    else if (allocated(ovarv(it)%fld2d) ) then
      ichck=1
      iv=iv+1
      idim=0
      do is=1,ismx
        ctmp=ovar(it)%dim(is)
        if (ctmp /= cna) then
          idim=idim+1
          do nd=1,ndim
            if (trim(ctmp) == trim(dim(nd)%name) ) then
              dimd2d(idim)=lvl_varid(nd)
            end if
          end do
        end if
      end do
      call check(nf90_def_var(ncid,trim(ovar(it)%name), &
                     nf90_real, dimd2d, var_varid(iv)) )
    else if (allocated(ovarv(it)%fld3d) ) then
      ichck=1
      iv=iv+1
      idim=0
      do is=1,ismx
        ctmp=ovar(it)%dim(is)
        if (ctmp /= cna) then
          idim=idim+1
          do nd=1,ndim
            if (trim(ctmp) == trim(dim(nd)%name) ) then
              dimd3d(idim)=lvl_varid(nd)
            end if
          end do
        end if
      end do
      call check(nf90_def_var(ncid,trim(ovar(it)%name), &
                     nf90_real, dimd3d, var_varid(iv)) )
    else if (allocated(ovarv(it)%fld4d) ) then
      ichck=1
      iv=iv+1
      idim=0
      do is=1,ismx
        ctmp=ovar(it)%dim(is)
        if (ctmp /= cna) then
          idim=idim+1
          do nd=1,ndim
            if (trim(ctmp) == trim(dim(nd)%name) ) then
              dimd4d(idim)=lvl_varid(nd)
            end if
          end do
        end if
      end do
      call check(nf90_def_var(ncid,trim(ovar(it)%name), &
                     nf90_real, dimd4d, var_varid(iv)) )
    end if
    !
    !       * assign attributes to the variable.
    !
    if (ichck == 1) then
      call check(nf90_put_att(ncid, var_varid(iv), "units", &
                     trim(ovar(it)%units) ) )
      call check(nf90_put_att(ncid, var_varid(iv), "long_name", &
                     trim(ovar(it)%lname) ) )
      call check(nf90_put_att(ncid, var_varid(iv), "_FillValue", &
                     ynax) )
    end if
  end do
  !
  !     * end define mode.
  !
  call check(nf90_enddef(ncid) )
  !
  !     * write the level data.
  !
  do nd=1,ndim
    if (dim(nd)%flgo == 1) then
      call check(nf90_put_var(ncid, lvl_varid(nd), dim(nd)%val) )
    end if
  end do
  !
  !     * write data for each field.
  !
  iv=0
  do it=1,itot
    if (abs(ovarv(it)%fld0d+ynax) > ysmallx) then
      iv=iv+1
      call check(nf90_put_var(ncid, var_varid(iv), &
                     ovarv(it)%fld0d) )
    else if (allocated(ovarv(it)%fld1d) ) then
      iv=iv+1
      call check(nf90_put_var(ncid, var_varid(iv), &
                     ovarv(it)%fld1d) )
    else if (allocated(ovarv(it)%fld2d) ) then
      iv=iv+1
      call check(nf90_put_var(ncid, var_varid(iv), &
                     ovarv(it)%fld2d) )
    else if (allocated(ovarv(it)%fld3d) ) then
      iv=iv+1
      call check(nf90_put_var(ncid, var_varid(iv), &
                     ovarv(it)%fld3d) )
    else if (allocated(ovarv(it)%fld4d) ) then
      iv=iv+1
      call check(nf90_put_var(ncid, var_varid(iv), &
                     ovarv(it)%fld4d) )
    end if
  end do
  !
  !     * close the file.
  !
  call check(nf90_close(ncid) )
  !
contains
  subroutine check(status)
    integer, intent (in) :: status !<

    if (status /= nf90_noerr) then
      print *, trim(nf90_strerror(status))
      stop 2
    end if
  end subroutine check
end subroutine wncdat
