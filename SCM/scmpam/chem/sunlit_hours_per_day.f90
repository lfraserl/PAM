!> \file
!> \brief Compute the number of sunlit hours in a day at a given latitude
!!
!! @author Routine author name(s)
!
subroutine sunlit_hours_per_day(latitude, & ! input
                                sin_declination, &
                                cos_declination, &
                                il1, &
                                il2, &
                                ilg, &
                                frac_sunlit_hours) ! output
  implicit none
  ! parameters
  real, parameter :: pi=3.1415926 !<
  real, parameter :: rad2deg = 180.0/pi !<
  real, parameter :: hours_per_day = 24.0 ! hours per day [hours]
  real, parameter :: earth_rotation_rate = 15.0 ! rotation rate of earth [degrees/hour]
  ! input
  integer, intent(in) :: il1 !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2 !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg !< Total number of atmospheric columns \f$[unitless]\f$
  real, intent(in)    :: sin_declination !< Sine   of declination angle for current timestep \f$[unitless]\f$
  real, intent(in)    :: cos_declination !< Cosine of declination angle for current timestep \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: latitude !< Latitude for atmospheric columns \f$[degrees (radians?)]\f$
  ! output
  real, intent(out), dimension(ilg) :: frac_sunlit_hours !< Fraction of sunlit hours in a day for current latitude and timestep \f$[unitless]\f$
  ! local
  integer :: il !<
  real :: cos_sunrise_hour_angle ! cosine of the solar hour angle at sunrise [unitless]
  real :: sunrise_hour_angle ! solar hour angle at sunrise [degrees]
  real, dimension(ilg) :: sunlit_hours ! number of sunlit hours [hours]
  ! initialize arrays
  sunlit_hours      = 0.0
  frac_sunlit_hours = 0.0
  do il=il1,il2
    cos_sunrise_hour_angle = -tan(latitude(il)) * (sin_declination/cos_declination)
    if (cos_sunrise_hour_angle <= -1.0) then ! sun is up all day
      sunlit_hours(il) = hours_per_day
    else if (cos_sunrise_hour_angle >= 1.0) then ! sun is down all day
      sunlit_hours(il) = 0.0
    else ! sun is up part of the day
      ! compute the hour angle of the sunrise
      sunrise_hour_angle = acos(cos_sunrise_hour_angle) * rad2deg

      ! the portion of the day where the sun is up is symmetric about local solar noon.
      ! if one converts the sunrise solar angle to solar time, then assume
      ! it is symmetric then one get the result below. e.g., the same time passes between
      ! sunrise and local solar noon and then local solar noon and sunset.
      sunlit_hours(il) = 2.0 * sunrise_hour_angle / earth_rotation_rate
    end if ! cos_sunrise_hour_angle
  end do ! il
  ! scale the number of sunlit hours by the number of hours per day.
  frac_sunlit_hours = sunlit_hours / hours_per_day
  return
end subroutine sunlit_hours_per_day
!> \file
!> This routine computes the number of sunlit hours at a particular latitude for a given timestep.
! this is done using the equations in the following reference,
!\n
! forsythe et al., a model comparison for daylength as a function of latitude and day of year, ecological :: modelling, 1995.
! https://www.sciencedirect.com/science/article/pii/030438009400034f
! https://gist.github.com/anttilipp/ed3ab35258c7636d87de6499475301ce
