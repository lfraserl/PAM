!> \file
!> \brief Adjust aerosol size distribution for internally mixed aerosol
!>       in response to complete removal of black carbon.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine extac (pidn,pidm,pidf,dirsmn,dirsn,dirsms, &
                        pimas,pinum,pin0,piphi0,pipsi,piddn,pifrc, &
                        iina,ita,kinta,isinta,ias,ilga,leva, &
                        ierr)
  !
  use sdparm, only : aintf,isaint,kint,pidphis,piphiss,ytiny
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(out) :: ierr !< Warning code index
  real, intent(out), dimension(ilga,leva) :: dirsmn !< Mass residuum for non-(NH4)2SO4 aerosol species from num.
  !< truncation in growth calculations \f$[kg/kg]\f$, Int. mixture
  real, intent(out), dimension(ilga,leva) :: dirsn !< Number residuum from numerical truncation in growth
  !< calculations \f$[kg/kg]\f$, Int. mixture
  real, intent(out), dimension(ilga,leva) :: dirsms !< Mass residuum for (NH4)2SO4 aerosol from num. truncation
  !< in growth calculations \f$[kg/kg]\f$, Int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidn !< Tracer number concentration
  real, intent(out), dimension(ilga,leva,isaint) :: pidm !< Tracer mass concentratioon
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidf !< Tracer mass fraction
  integer, intent(in), dimension(isinta) :: iina !< Index of aerosol tracers in aerosol mass and number arrays.
  integer, intent(in), dimension(ias) :: ita !< Aerosol tracer index for additional chemical species
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piddn !< (Dry) density for internally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  integer, parameter :: imodc=2 !<
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:,:) :: timn !<
  real, allocatable, dimension(:,:,:,:) :: tifrcn !<
  real, allocatable, dimension(:,:,:) :: tin0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: tiddn !<
  real, allocatable, dimension(:,:,:) :: tifrcs !<
  real, allocatable, dimension(:,:,:) :: ticgr1 !<
  real, allocatable, dimension(:,:,:) :: ticgr2 !<
  real, allocatable, dimension(:,:,:) :: titrm1 !<
  real, allocatable, dimension(:,:,:) :: titrm2 !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3s !<
  real(r8), allocatable, dimension(:,:,:) :: timst !<
  real(r8), allocatable, dimension(:,:,:,:) :: timnt !<
  real, allocatable, dimension(:,:,:) :: tin !<
  real, allocatable, dimension(:,:,:) :: tint !<
  real, allocatable, dimension(:,:,:) :: tim !<
  real, allocatable, dimension(:,:,:) :: timt !<
  real, allocatable, dimension(:,:,:) :: timr !<
  real, allocatable, dimension(:,:,:) :: tirsmn !<
  real, allocatable, dimension(:,:,:) :: titm1 !<
  real, allocatable, dimension(:,:,:) :: titm2 !<
  real, allocatable, dimension(:,:,:) :: tidryr !<
  real, allocatable, dimension(:,:,:) :: tidphis !<
  real, allocatable, dimension(:,:,:) :: tiphiss !<
  real, allocatable, dimension(:,:) :: term1 !<
  real, allocatable, dimension(:,:) :: term2 !<
  real, allocatable, dimension(:,:) :: term3 !<
  real, allocatable, dimension(:,:) :: term4 !<
  integer, allocatable, dimension(:,:,:) :: tii0l !<
  integer, allocatable, dimension(:,:,:) :: tii0r !<
  integer :: ias !<
  integer :: il !<
  integer :: is !<
  integer :: it !<
  integer :: isinta !<
  integer :: l !<
  integer :: kinta !<
  real :: cfn !<
  real :: cfm !<


  !
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  if (isaint > 0) then
    pidn=0.
    pidm=0.
    pidf=0.
  end if
  ierr=0
  !
  !-----------------------------------------------------------------------
  !     * allocate memory for local fields.
  !
  if (isinta > 0) then
    allocate(titrm1 (ilga,leva,isinta))
    allocate(titrm2 (ilga,leva,isinta))
    allocate(ticgr1 (ilga,leva,isinta))
    allocate(ticgr2 (ilga,leva,isinta))
    allocate(timn   (ilga,leva,isinta,ias))
    allocate(timnt  (ilga,leva,isinta,ias))
    allocate(tifrcn (ilga,leva,isinta,ias))
    allocate(tifrcs (ilga,leva,isinta))
    allocate(tin0   (ilga,leva,isinta))
    allocate(tiphi0 (ilga,leva,isinta))
    allocate(tipsi  (ilga,leva,isinta))
    allocate(tiddn  (ilga,leva,isinta))
    allocate(tim    (ilga,leva,isinta))
    allocate(timr   (ilga,leva,isinta))
    allocate(timt   (ilga,leva,isinta))
    allocate(tin    (ilga,leva,isinta))
    allocate(tint   (ilga,leva,isinta))
    allocate(timst  (ilga,leva,isinta))
    allocate(tii0l  (ilga,leva,isinta))
    allocate(tii0r  (ilga,leva,isinta))
    allocate(tidp0l (ilga,leva,isinta))
    allocate(tidp1l (ilga,leva,isinta))
    allocate(tidp2l (ilga,leva,isinta))
    allocate(tidp3l (ilga,leva,isinta))
    allocate(tidp4l (ilga,leva,isinta))
    allocate(tidp5l (ilga,leva,isinta))
    allocate(tidp6l (ilga,leva,isinta))
    allocate(tidm1l (ilga,leva,isinta))
    allocate(tidm2l (ilga,leva,isinta))
    allocate(tidm3l (ilga,leva,isinta))
    allocate(tidp0r (ilga,leva,isinta))
    allocate(tidp1r (ilga,leva,isinta))
    allocate(tidp2r (ilga,leva,isinta))
    allocate(tidp3r (ilga,leva,isinta))
    allocate(tidp4r (ilga,leva,isinta))
    allocate(tidp5r (ilga,leva,isinta))
    allocate(tidp6r (ilga,leva,isinta))
    allocate(tidm1r (ilga,leva,isinta))
    allocate(tidm2r (ilga,leva,isinta))
    allocate(tidm3r (ilga,leva,isinta))
    allocate(tidp0s (ilga,leva,isinta))
    allocate(tidp1s (ilga,leva,isinta))
    allocate(tidp2s (ilga,leva,isinta))
    allocate(tidp3s (ilga,leva,isinta))
    allocate(tidp4s (ilga,leva,isinta))
    allocate(tidp5s (ilga,leva,isinta))
    allocate(tidp6s (ilga,leva,isinta))
    allocate(tidm1s (ilga,leva,isinta))
    allocate(tidm2s (ilga,leva,isinta))
    allocate(tidm3s (ilga,leva,isinta))
    allocate(tirsmn (ilga,leva,ias))
    allocate(titm1  (ilga,leva,ias))
    allocate(titm2  (ilga,leva,ias))
    allocate(tidphis(ilga,leva,isinta))
    allocate(tiphiss(ilga,leva,isinta))
    allocate(tidryr (ilga,leva,isinta+1))
  end if
  allocate(term1(ilga,leva))
  allocate(term2(ilga,leva))
  allocate(term3(ilga,leva))
  allocate(term4(ilga,leva))
  !
  !-----------------------------------------------------------------------
  !     * temporary aerosol fields for dry and wet particle sizes
  !     * for internally mixed aerosol type.
  !
  if (kinta > 0) then
    do is=1,isinta
      tin    (:,:,is)=pinum  (:,:,iina(is))
      tim    (:,:,is)=pimas  (:,:,iina(is))
      tifrcs (:,:,is)=pifrc  (:,:,iina(is),kinta)
      do it=1,ias
        timn(:,:,is,it)=pimas  (:,:,iina(is)) &
                           *pifrc  (:,:,iina(is),ita(it))
        tifrcn(:,:,is,it)=pifrc(:,:,iina(is),ita(it))
      end do
      tin0   (:,:,is)=pin0   (:,:,iina(is))
      tiphi0 (:,:,is)=piphi0 (:,:,iina(is))
      tipsi  (:,:,is)=pipsi  (:,:,iina(is))
      tiddn  (:,:,is)=piddn  (:,:,iina(is))
      tidphis(:,:,is)=pidphis(iina(is))
      tiphiss(:,:,is)=piphiss(iina(is))
      tidryr (:,:,is)=aintf%dryr(iina(is))%vl
    end do
    timr(:,:,:)=sum(timn,dim=4)
    tidryr(:,:,isinta+1)=aintf%dryr(aintf%tp(kinta)%isce)%vr
  end if
  !
  !-----------------------------------------------------------------------
  !     * growth factor from r(t)=r(t=0)*c, with c=cgr1+cgr2*r(t=0),
  !     * for dry particle radius r and time t. it is assumed that
  !     * the relative increase in dry particle radius for each section
  !     * equals (vt/v)**(1/3), where v is the initial total volume of
  !     * the aerosol and vt is the volume after the extraction of the
  !     * aerosol species. here, changes in dry density are not
  !     * accounted for. they will be considered in subsequent
  !     * parameterizations.
  !
  titrm2=1./3.
  if (isinta > 0) then
    !
    !       * aerosol volume concentration fraction.
    !
    titrm1=1.
    where (tim > ytiny)
      titrm1=timr/tim
    end where
    !
    !       * aerosol radius change, limited to particles larger
    !       * than smallest possible particle size.
    !
    ticgr2=0.
    ticgr1=titrm1**titrm2
  end if
  !
  !-----------------------------------------------------------------------
  !     * the following calculations provide the updated size
  !     * distributions for particle number, sulphur aerosol mass,
  !     * and mass of other aerosol species after the volume adjustment.
  !
  dirsn=0.
  dirsms=0.
  dirsmn=0.
  if (isinta > 0) then
    call pgrpar (tidp0l,tidp1l,tidp2l,tidp3l,tidp4l,tidp5l,tidp6l, &
                     tidm1l,tidm2l,tidm3l,tidp0r,tidp1r,tidp2r,tidp3r, &
                     tidp4r,tidp5r,tidp6r,tidm1r,tidm2r,tidm3r,tidp0s, &
                     tidp1s,tidp2s,tidp3s,tidp4s,tidp5s,tidp6s,tidm1s, &
                     tidm2s,tidm3s,tii0l,tii0r,ticgr1,ticgr2,tin0, &
                     tidryr,tiphiss,tidphis,tiphi0,tipsi,ilga,leva, &
                     isinta,ierr,imodc)
    call pgrwth0(tint,dirsn,tii0l,tii0r,tidp0l,tidp0r,tidp0s, &
                     tin0,ilga,leva,isinta)
    call pgrwthc(timst,dirsms,tifrcs,tii0l,tii0r, &
                     tidp3l,tidp4l,tidp5l,tidp6l,tidp3r,tidp4r,tidp5r, &
                     tidp6r,tidp3s,tidp4s,tidp5s,tidp6s, &
                     ticgr1,ticgr2,tin0,tiddn,ilga,leva,isinta)
    if (ias > 0) then
      tirsmn=0.
      call pgrwthn(timnt,tirsmn,tifrcn,tii0l,tii0r,tidp3l,tidp3r, &
                       tidp3s,tin0,tiddn,ilga,leva,ias,isinta)
      dirsmn=sum(tirsmn,dim=3)
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * number and mass fixers. this corrects for any truncation
  !     * errors that may have occurred previously. the approach is
  !     * to scale the total number and mass concentrations so that
  !     * truncation errors will be reduced.
  !
  term1=0.
  term2=0.
  term3=0.
  term4=0.
  if (isinta > 0) then
    tint=max(tint,0.)
    timst=max(timst,0._r8)
    term1=term1+sum(timst,dim=3)+dirsms
    term2=term2+sum(tint ,dim=3)+dirsn
    term4=term4+sum(tin  ,dim=3)
    if (ias > 0) then
      titm1=sum(timnt,dim=3)+tirsmn
      titm2=sum(timn ,dim=3)
    end if
  end if
  !
  do l=1,leva
    do il=1,ilga
      !
      !       * correct aerosol number concentration.
      !
      if (2+abs(exponent(term4(il,l)) - exponent(term2(il,l)) ) &
          < maxexponent(term4(il,l)) .and. term2(il,l)/=0. ) then
        cfn=term4(il,l)/term2(il,l)
        if (isinta > 0) tint(il,l,:)=tint(il,l,:)*cfn
      end if
      !
      !       * correct extracted aerosol mass concentration.
      !
      if (2+abs(exponent(term3(il,l)) - exponent(term1(il,l))) &
          < maxexponent(term3(il,l)) .and. term1(il,l)/=0. ) then
        cfm=term3(il,l)/term1(il,l)
        if (isinta > 0) timst(il,l,:)=timst(il,l,:)*cfm
      end if
    end do
  end do
  !
  !     * correct non-extracted aerosol mass concentration.
  !
  if (isinta > 0) then
    do it=1,ias
      do l=1,leva
        do il=1,ilga
          if (2+abs(exponent(titm2(il,l,it)) - exponent(titm1(il,l,it))) &
              < maxexponent(titm2(il,l,it)) .and. titm1(il,l,it)/=0. ) then
            cfm=titm2(il,l,it)/titm1(il,l,it)
            timnt(il,l,:,it)=timnt(il,l,:,it)*cfm
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * total aerosol mass for internally mixed aerosol species.
  !
  if (kinta > 0) then
    timt=timst
    do it=1,ias
      timt(:,:,:)=timt(:,:,:)+timnt(:,:,:,it)
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * tracer tendencies.
  !
  if (kinta > 0) then
    do is=1,isinta
      pidn(:,:,iina(is))=(tint(:,:,is)-tin(:,:,is))
      pidm(:,:,iina(is))=(timt(:,:,is)-tim(:,:,is))
      where (2+abs(exponent(timst(:,:,is)) - exponent(timt(:,:,is))) &
          < maxexponent(timst(:,:,is)) .and. timt(:,:,is)/=0. )
        pidf(:,:,iina(is),kinta)= &
                                      (timst(:,:,is)/timt(:,:,is)-tifrcs(:,:,is))
      else where
        pidf(:,:,iina(is),kinta)=-tifrcs(:,:,is)
      end where
      do it=1,ias
        where (2+abs(exponent(timnt(:,:,is,it)) - exponent(timt(:,:,is))) &
            < maxexponent(timnt(:,:,is,it)) .and. timt(:,:,is)/=0. )
          pidf(:,:,iina(is),ita(it))= &
                                          (timnt(:,:,is,it)/timt(:,:,is)-tifrcn(:,:,is,it))
        else where
          pidf(:,:,iina(is),ita(it))=-tifrcn(:,:,is,it)
        end where
      end do
    end do
  end if
  !
  !     * deallocation.
  !
  if (isinta > 0) then
    deallocate(titrm1)
    deallocate(titrm2)
    deallocate(ticgr1)
    deallocate(ticgr2)
    deallocate(timn)
    deallocate(timnt)
    deallocate(tifrcn)
    deallocate(tifrcs)
    deallocate(tin0)
    deallocate(tiphi0)
    deallocate(tipsi)
    deallocate(tiddn)
    deallocate(tim)
    deallocate(timr)
    deallocate(timt)
    deallocate(tin)
    deallocate(tint)
    deallocate(timst)
    deallocate(tii0l)
    deallocate(tii0r)
    deallocate(tidp0l)
    deallocate(tidp1l)
    deallocate(tidp2l)
    deallocate(tidp3l)
    deallocate(tidp4l)
    deallocate(tidp5l)
    deallocate(tidp6l)
    deallocate(tidm1l)
    deallocate(tidm2l)
    deallocate(tidm3l)
    deallocate(tidp0r)
    deallocate(tidp1r)
    deallocate(tidp2r)
    deallocate(tidp3r)
    deallocate(tidp4r)
    deallocate(tidp5r)
    deallocate(tidp6r)
    deallocate(tidm1r)
    deallocate(tidm2r)
    deallocate(tidm3r)
    deallocate(tidp0s)
    deallocate(tidp1s)
    deallocate(tidp2s)
    deallocate(tidp3s)
    deallocate(tidp4s)
    deallocate(tidp5s)
    deallocate(tidp6s)
    deallocate(tidm1s)
    deallocate(tidm2s)
    deallocate(tidm3s)
    deallocate(tirsmn)
    deallocate(titm1)
    deallocate(titm2)
    deallocate(tidphis)
    deallocate(tiphiss)
    deallocate(tidryr)
  end if
  deallocate(term1)
  deallocate(term2)
  deallocate(term3)
  deallocate(term4)
  !
end subroutine extac
