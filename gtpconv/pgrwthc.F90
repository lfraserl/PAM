!> \file
!> \brief Particle growth from in-situ chemical production of aerosol mass
!>       with an associated change in particle radius R -> C*R.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine pgrwthc (tmas,resm,frc,i0l,i0r, &
                          dp3l,dp4l,dp5l,dp6l,dp3r,dp4r,dp5r, &
                          dp6r,dp3s,dp4s,dp5s,dp6s, &
                          cgr1,cgr2,pn0,drydn,ilga,leva,isec)
  !
  use sdparm, only : ycnst,ytiny
  use fpdef,  only : r8
  !
  implicit none
  !
  real(r8), intent(out), dimension(ilga,leva,isec) :: tmas !< Mass size distribution
  real, intent(inout), dimension(ilga,leva) :: resm !< Mass residuum from numerical truncation in growth calculations
  real, intent(in), dimension(ilga,leva,isec) :: cgr1 !< Growth rate
  real, intent(in), dimension(ilga,leva,isec) :: cgr2 !< Growth rate
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: frc !< Fraction
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp4l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp5l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp6l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp4r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp5r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp6r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp4s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp5s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp6s !<
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  integer, intent(in), dimension(ilga,leva,isec) :: i0l !< Left-hand side part of the modified section
  integer, intent(in), dimension(ilga,leva,isec) :: i0r !< Right-hand side part of the modified section
  !
  !     internal work variables
  !
  real(r8) :: c0 !<
  real(r8) :: c1 !<
  real(r8) :: c2 !<
  real(r8) :: tp3l !<
  real(r8) :: tp4l !<
  real(r8) :: tp5l !<
  real(r8) :: tp6l !<
  real(r8) :: tp3r !<
  real(r8) :: tp4r !<
  real(r8) :: tp5r !<
  real(r8) :: tp6r !<
  real(r8) :: tp3s !<
  real(r8) :: tp4s !<
  real(r8) :: tp5s !<
  real(r8) :: tp6s !<
  integer :: il !<
  integer :: is !<
  integer :: isi !<
  integer :: l !<
  real :: atmp !<
  real :: ct !<
  !
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  tmas=0.
  !
  !-----------------------------------------------------------------------
  !     calculate number and mass in each section.
  !
  do isi=1,isec
    do l=1,leva
      do il=1,ilga
        !
        !       calculate contribution in each section from the left-hand
        !       side part of the modified section.
        !
        is=i0l(il,l,isi)
        if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
            ) then
          !
          !         mass size distribution from
          !         m(phi(t))=f*(c*r(t=0))**3*n(phi(t=0))
          !
          ct=pn0(il,l,isi)*drydn(il,l,isi)*ycnst
          c0=ct*frc(il,l,isi)
          c1=cgr1(il,l,isi)
          c2=cgr2(il,l,isi)
          tp3l=dp3l(il,l,isi)
          tp4l=dp4l(il,l,isi)
          tp5l=dp5l(il,l,isi)
          tp6l=dp6l(il,l,isi)
          if (abs(c2) > ytiny) then
            atmp=3._r8*tp4l*c2*c1**2+3._r8*tp5l*c1*c2**2+tp6l*c2**3
          else
            atmp=0.
          end if
          tmas(il,l,is)=tmas(il,l,is)+c0*tp3l+ct*(tp3l*(c1**3-1.) &
                                                                +atmp)
        end if
        !
        !        calculate contribution in each section from the right-hand
        !        side part of the modified section.
        !
        is=i0r(il,l,isi)
        if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
            .and. i0l(il,l,isi) /= is) then
          !
          !          mass size distribution from
          !          m(phi(t))=f*(c*r(t=0))**3*n(phi(t=0))
          !
          ct=pn0(il,l,isi)*drydn(il,l,isi)*ycnst
          c0=ct*frc(il,l,isi)
          c1=cgr1(il,l,isi)
          c2=cgr2(il,l,isi)
          tp3r=dp3r(il,l,isi)
          tp4r=dp4r(il,l,isi)
          tp5r=dp5r(il,l,isi)
          tp6r=dp6r(il,l,isi)
          if (abs(c2) > ytiny) then
            atmp=3._r8*tp4r*c2*c1**2+3._r8*tp5r*c1*c2**2+tp6r*c2**3
          else
            atmp=0.
          end if
          tmas(il,l,is)=tmas(il,l,is)+c0*tp3r+ct*(tp3r*(c1**3-1.) &
                                                                +atmp)
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     residuum due to particles growing to sizes larger than
  !     the spectrum cutoff.
  !
  do isi=1,isec
    do l=1,leva
      do il=1,ilga
        if ( (i0r(il,l,isi) == (isec+1) .or. i0l(il,l,isi) == 0) &
            .and. pn0(il,l,isi) > ytiny) then
          !
          !        mass size distribution from
          !        m(phi(t))=f*(c*r(t=0))**3*n(phi(t=0))
          !
          ct=pn0(il,l,isi)*drydn(il,l,isi)*ycnst
          c0=ct*frc(il,l,isi)
          c1=cgr1(il,l,isi)
          c2=cgr2(il,l,isi)
          tp3s=dp3s(il,l,isi)
          tp4s=dp4s(il,l,isi)
          tp5s=dp5s(il,l,isi)
          tp6s=dp6s(il,l,isi)
          if (abs(c2) > ytiny) then
            atmp=3._r8*tp4s*c2*c1**2+3._r8*tp5s*c1*c2**2+tp6s*c2**3
          else
            atmp=0.
          end if
          resm(il,l)=resm(il,l)+c0*tp3s+ct*(tp3s*(c1**3-1.)+atmp)
        end if
      end do
    end do
  end do
  !
end subroutine pgrwthc
!> \file
!! \subsection ssec_details Details
!! It is assumed that sections initially have the same size and that
!! growth leads to a an equal or smaller width of each section.
!! the subroutine treats growth in particle size space based on
!! a Lagrangian approach.

