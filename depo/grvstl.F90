!>    \file
!>    \brief Gravitational settling of aerosol particles.
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine grvstl(pedndt,pedmdt,pidfdt,pidndt,pidmdt,penum,pemas, &
                        pen0,pephi0,pepsi,pephis0,pedphi0,peddn, &
                        pewetrc,pifrc,pinum,pimas,pin0,piphi0, &
                        pipsi,piphis0,pidphi0,piddn,piwetrc, &
                        t,p,dp,rhoa,grav,dt,ilga,leva)
  !
  use sdparm, only : isaext,isaint,kint,pedryrc,pidryrc,r0,ytiny
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !< Number tendency \f$[kg/kg/sec]\f$, external mixture
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !< Mass tendency \f$[kg/kg/sec]\f$, external mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !< Number tendency \f$[kg/kg/sec]\f$, internal mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !< Mass tendency \f$[kg/kg/sec]\f$, internal mixture
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !< Aerosol species mass fraction tendency \f$[1/sec]\f$, internal mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !< Dry particle radius in the centres of the size
  !< sections for externally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !< Total/wet paricle radius \f$[m]\f$, ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, external mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !< Dry particle radius in the centres of the size
  !< sections for internally mixed aerosol \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: piwetrc !< Total/wet paricle radius \f$[m\f$, int. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: piddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, internal mixture
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol species mass fraction
  real, intent(in), dimension(ilga,leva) :: t !< Air temperature \f$[K]\f$
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva) :: dp !< Depth of grid cells \f$[Pa]\f$
  real, intent(in), dimension(ilga,leva) :: p !< Air pressure \f$[Pa]\f$
  real, intent(in) :: grav !< Gravtiational accelaration \f$[m/s^2]\f$
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: pefn !<
  real, allocatable, dimension(:,:,:) :: pefm !<
  real, allocatable, dimension(:,:,:) :: pegsvdn !<
  real, allocatable, dimension(:,:,:) :: pegsvdm !<
  real, allocatable, dimension(:,:,:) :: pifn !<
  real, allocatable, dimension(:,:,:) :: pifm !<
  real, allocatable, dimension(:,:,:) :: pigsvdn !<
  real, allocatable, dimension(:,:,:) :: pigsvdm !<
  real, allocatable, dimension(:,:,:,:) :: pifms !<
  real, allocatable, dimension(:,:,:,:) :: pidmdts !<
  real, allocatable, dimension(:,:,:,:) :: pimass !<
  real, allocatable, dimension(:,:,:,:) :: pifrcs !<
  real, allocatable, dimension(:,:) :: pesfn !<
  real, allocatable, dimension(:,:) :: pesfm !<
  real, allocatable, dimension(:,:) :: pisfn !<
  real, allocatable, dimension(:,:) :: pisfm !<
  real, allocatable, dimension(:,:,:) :: pisfms !<
  real, allocatable, dimension(:,:,:) :: pimasst !<
  real, dimension(ilga,leva) :: amu !<
  real, dimension(ilga,leva) :: amfp !<
  real, dimension(ilga,leva) :: term1 !<
  real, dimension(ilga,leva) :: vcfl !<
  integer :: is !<
  integer :: it !<
  integer :: l !<
  logical, parameter :: ksrmv=.true. ! surface removal
  !
  !-----------------------------------------------------------------------
  !     * allocate work space.
  !
  if (isaext > 0) then
    allocate(pefn   (ilga,leva,isaext))
    allocate(pefm   (ilga,leva,isaext))
    allocate(pegsvdn(ilga,leva,isaext))
    allocate(pegsvdm(ilga,leva,isaext))
    allocate(pesfn  (ilga,isaext))
    allocate(pesfm  (ilga,isaext))
  end if
  if (isaint > 0) then
    allocate(pifrcs (ilga,leva,isaint,kint))
    allocate(pimass (ilga,leva,isaint,kint))
    allocate(pidmdts(ilga,leva,isaint,kint))
    allocate(pifms  (ilga,leva,isaint,kint))
    allocate(pimasst(ilga,leva,isaint))
    allocate(pisfms (ilga,isaint,kint))
    allocate(pifn   (ilga,leva,isaint))
    allocate(pifm   (ilga,leva,isaint))
    allocate(pigsvdn(ilga,leva,isaint))
    allocate(pigsvdm(ilga,leva,isaint))
    allocate(pisfn  (ilga,isaint))
    allocate(pisfm  (ilga,isaint))
  end if
  !
  !-----------------------------------------------------------------------
  !     * dynamic viscosity of air.
  !
  term1=sqrt(t)
  term1=term1*t
  amu=145.8*1.e-8*term1/(t+110.4)
  !
  !     * mean free path (k.v. beard, 1976) and cunningham slip
  !     * correction factor.
  !
  term1=sqrt(t/293.15)
  amfp=6.54e-8*(amu/1.818e-5)*(1.013e5/p)*term1
  !
  !-----------------------------------------------------------------------
  !     * maximum fall velocity according to cfl criterion.
  !
  vcfl=.999*dp/(rhoa*grav*dt)
  !
  !-----------------------------------------------------------------------
  !     * terminal fall velocities.
  !
  if (isaext > 0) &
      call gspar(pegsvdn,pegsvdm,rhoa,vcfl,amu,amfp,penum,pemas,pen0, &
      pephi0,pepsi,pephis0,pedphi0,peddn,pewetrc, &
      pedryrc,grav,ilga,leva,isaext)
  if (isaint > 0) &
      call gspar(pigsvdn,pigsvdm,rhoa,vcfl,amu,amfp,pinum,pimas,pin0, &
      piphi0,pipsi,piphis0,pidphi0,piddn,piwetrc, &
      pidryrc,grav,ilga,leva,isaint)
  !
  !-----------------------------------------------------------------------
  !     * tracer fluxes at grid cell interfaces.
  !
  l=leva
  do is=1,isaext
    pesfn(:,  is)=rhoa(:,l)*pegsvdn(:,l,is)*penum(:,l,is)
    pefn (:,:,is)=rhoa(:,:)*pegsvdn(:,:,is)*penum(:,:,is)
    pesfm(:,  is)=rhoa(:,l)*pegsvdm(:,l,is)*pemas(:,l,is)
    pefm (:,:,is)=rhoa(:,:)*pegsvdm(:,:,is)*pemas(:,:,is)
  end do
  l=leva
  do is=1,isaint
    pisfn(:,  is)=rhoa(:,l)*pigsvdn(:,l,is)*pinum(:,l,is)
    pifn (:,:,is)=rhoa(:,:)*pigsvdn(:,:,is)*pinum(:,:,is)
    do it=1,kint
      pisfms(:,  is,it)=rhoa(:,l)*pigsvdm(:,l,is) &
                                        *pifrc(:,l,is,it)*pimas(:,l,is)
      pifms (:,:,is,it)=rhoa(:,:)*pigsvdm(:,:,is) &
                                        *pifrc(:,:,is,it)*pimas(:,:,is)
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * tendencies from upstream calculations.
  !
  do is=1,isaext
    l=1
    pedndt(:,l,is)=grav*(-pefn(:,l,is))/dp(:,l)
    pedmdt(:,l,is)=grav*(-pefm(:,l,is))/dp(:,l)
    do l=2,leva-1
      pedndt(:,l,is)=grav*(pefn(:,l-1,is)-pefn(:,l,is))/dp(:,l)
      pedmdt(:,l,is)=grav*(pefm(:,l-1,is)-pefm(:,l,is))/dp(:,l)
    end do
  end do
  do is=1,isaint
    l=1
    pidndt(:,l,is)=grav*(-pifn(:,l,is))/dp(:,l)
    do it=1,kint
      pidmdts(:,l,is,it)=grav*(-pifms(:,l,is,it))/dp(:,l)
    end do
    do l=2,leva-1
      pidndt(:,l,is)=grav*(pifn(:,l-1,is)-pifn(:,l,is))/dp(:,l)
      do it=1,kint
        pidmdts(:,l,is,it)= &
                                grav*(pifms(:,l-1,is,it)-pifms(:,l,is,it))/dp(:,l)
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * surface removal. note that this part should be included in
  !     * the parameterization of dry deposition in later model versions !
  !
  if (ksrmv) then
    l=leva
    do is=1,isaext
      pedndt(:,l,is)=grav*(pefn(:,l-1,is)-pesfn(:,is))/dp(:,l)
      pedmdt(:,l,is)=grav*(pefm(:,l-1,is)-pesfm(:,is))/dp(:,l)
    end do
    do is=1,isaint
      pidndt(:,l,is)=grav*(pifn(:,l-1,is)-pisfn(:,is))/dp(:,l)
      do it=1,kint
        pidmdts(:,l,is,it)= &
                                grav*(pifms(:,l-1,is,it)-pisfms(:,is,it))/dp(:,l)
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * new mass concentrations for internally mixed aerosol after
  !     * gravitational settling.
  !
  if (isaint > 0) then
    do it=1,kint
      pimass(:,:,:,it)=pifrc(:,:,:,it)*pimas(:,:,:) &
                                                  +dt*pidmdts(:,:,:,it)
    end do
    do it=1,kint
      pimasst=sum(pimass,dim=4)
      where (2+abs(exponent(pimass(:,:,:,it)) - exponent(pimasst)) &
          < maxexponent(pimass(:,:,:,it)) .and. pimasst/=0. )
        pifrcs(:,:,:,it)=pimass(:,:,:,it)/pimasst
      else where
        pifrcs(:,:,:,it)=pifrc(:,:,:,it)
      end where
    end do
    pidmdt=(pimasst-pimas)/dt
    pidfdt=(pifrcs-pifrc)/dt
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate work space.
  !
  if (isaext > 0) then
    deallocate(pefn)
    deallocate(pefm)
    deallocate(pegsvdn)
    deallocate(pegsvdm)
    deallocate(pesfn)
    deallocate(pesfm)
  end if
  if (isaint > 0) then
    deallocate(pifrcs)
    deallocate(pimass)
    deallocate(pidmdts)
    deallocate(pifms)
    deallocate(pimasst)
    deallocate(pisfms)
    deallocate(pifn)
    deallocate(pifm)
    deallocate(pigsvdn)
    deallocate(pigsvdm)
    deallocate(pisfn)
    deallocate(pisfm)
  end if
  !
end subroutine grvstl
!> \file
!! \subsection ssec_details Details
!! It is necessary to provide consistent representations of the aerosol size
!! distributions in terms of number and mass concentrations,
!! PLA parameters, and dry particle density as input to this subroutine.
!! \n\n
!! PRIMARY INPUT/OUTPUT VARIABLES (ARGUMENT LIST):
!! |In/Out | Name   | Details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! |  I    | DP     | DIFFERENCE IN AIR PRESSURE BETWEEN BOTTOM AND TOP OF GRID CELL (PA)|
!! |  I    | DT     | TIME STEP (SEC)|
!! |  I    | GRAV   | GRAVTIATIONAL ACCELARATION (M/S2)|
!! |  I    | ILGA   | NUMBER OF GRID POINTS IN HORIZONTAL DIRECTION|
!! |  I    | LEVA   | NUMBER OF GRID POINTA IN VERTICAL DIRECTION|
!! |  I    | P      | AIR PRESSURE (PA)|
!! |  I    | PEDDN  | DENSITY OF DRY AEROSOL PARTICLE (KG/M3), EXT. MIXTURE|
!! |  O    | PEDMDT | MASS TENDENCY (KG/KG/SEC), EXT. MIXTURE|
!! |  O    | PEDNDT | NUMBER TENDENCY (1/KG/SEC), EXT. MIXTURE|
!! |  I    | PEDPHI0| SECTION WIDTH, EXT. MIXTURE|
!! |  I    | PEMAS  | AEROSOL MASS CONCENTRATION (KG/KG), EXT. MIXTURE|
!! |  I    | PEN0   | 1ST PLA SIZE DISTRIBUTION PARAMETER (AMPLITUDE), EXT. MIXTURE|
!! |  I    | PENUM  | AEROSOL NUMBER CONCENTRATION (1/KG), EXT. MIXTURE|
!! |  I    | PEPHI0 | 3RD PLA SIZE DISTRIBUTION PARAMETER (MODE SIZE), EXT. MIXTURE|
!! |  I    | PEPHIS0| PARTICLE SIZE AT THE LOWER SECTION BOUNDARY, EXT. MIXTURE|
!! |  I    | PEPSI  | 2ND PLA SIZE DISTRIBUTION PARAMETER (WIDTH), EXT. MIXTURE|
!! |  I    | PEWETRC| TOTAL/WET PARICLE RADIUS (M), EXT. MIXTURE|
!! |  I    | PIDDN  | DENSITY OF DRY AEROSOL PARTICLE (KG/M3), INT. MIXTURE|
!! |  O    | PIDFDT | AEROSOL SPECIES MASS FRACTION TENDENCY (1/SEC), INT. MIXTURE|
!! |  O    | PIDMDT | MASS TENDENCY (KG/KG/SEC), INT. MIXTURE|
!! |  O    | PIDNDT | NUMBER TENDENCY (1/KG/SEC), INT. MIXTURE|
!! |  I    | PIDPHI0| SECTION WIDTH, INT. MIXTURE|
!! |  I    | PIFRC  | AEROSOL SPECIES MASS FRACTION|
!! |  I    | PIMAS  | AEROSOL MASS CONCENTRATION (KG/KG), INT. MIXTURE|
!! |  I    | PIN0   | 1ST PLA SIZE DISTRIBUTION PARAMETER (AMPLITUDE), INT. MIXTURE|
!! |  I    | PINUM  | AEROSOL NUMBER CONCENTRATION (1/KG), INT. MIXTURE|
!! |  I    | PIPHI0 |  3RD PLA SIZE DISTRIBUTION PARAMETER (MODE SIZE), INT.MIXTURE|
!! |  I    | PIPHIS0|  PARTICLE SIZE AT THE LOWER SECTION BOUNDARY, INT. MIXTURE|
!! |  I    | PIPSI  |  2ND PLA SIZE DISTRIBUTION PARAMETER (WIDTH), INT. MIXTURE|
!! |  I    | PIWETRC|  TOTAL/WET PARICLE RADIUS (M), INT. MIXTURE|
!! |  I    | RH     |  RELATIVE HUMIDITY (DIMENSIONLESS)|
!! |  I    | T      |  AIR TEMPERATURE (K)|
!! \n\n
!! SECONDARY INPUT/OUTPUT VARIABLES (VIA MODULES AND COMMON BLOCKS):
!! |In/Out | Name   | Details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! |  I    | ISAEXT |  NUMBER OF AEROSOL TRACERS, EXT. MIXTURE|
!! |  I    | ISAINT |  NUMBER OF AEROSOL TRACERS, INT. MIXTURE|
!! |  I    | KINT   |  NUMBER OF INTERNALLY MIXED AEROSOL TYPES, INT. MIXTURE|
!! |  I    | YTINY  |  SMALLEST VALID NUMBER|
